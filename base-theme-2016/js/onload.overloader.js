
	function addOnloadEvent(func)
	{
		if (typeof func == 'function')
		{
			if (typeof window.onload == 'function')
			{
				var old_function = window.onload;
				window.onload = function(){
					old_function();
					func();
				};
			}
			else
				window.onload = func;
		}
	}