var winWidth;
$(window).load(function() {

    winWidth = checkWinWidth();

    if (winWidth < 768) {
        if ($('.single-product').length) {

            $('.details').find('.col-lg-8').prependTo($('.single-product .actions'));
            $('.actions').find('.col-lg-4').appendTo($('.single-product .details'));

        }
    }


});
var itemWidth = 0;
var resizeTimer;
var thumbBool = false;
var windowWidth
$(window).on('resize', function() {

    windowWidth = checkWinWidth();

    if (windowWidth > 767) {

        $('.feature-products').css('opacity', '0');
        featuredProductsResize();

    } else {

        $('.feature-products').find('.category a img').each(function() {

            var t = $(this);
            var primParent = $(t).parents('.primary-image');
            var secParent = $(t).parents('.secondary-image');

            $(t).css({

                'max-height': '',
                'min-height': ''

            })
            $(primParent).css({

                'max-height': '',
                'min-height': ''

            })
            $(secParent).css({

                'max-height': '',
                'min-height': ''

            })

        })

    }

    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {

        if ($('#slider-thumbs').length && thumbBool === false) {
            thumbBool = true;
        }

        if (thumbBool === true) {
            thumbs = $('#slider-thumbs').find('li');
            thumbsContainer = $('.thumbs-container');
            thumbsContainerWidth = $('#slider-thumbs').width();
            thumbPad = parseInt($('#slider-thumbs').find('li:first').css('padding-right'));
            noItems = $(thumbs).length;
            itemWidth = Math.round((thumbsContainerWidth / 3)) + thumbPad;
            slideWidth = (noItems * itemWidth);

            thumbs.each(function() {

                $(this).css({

                    'width': (itemWidth) + 'px'

                });

            });

            thumbsContainer.css('width', slideWidth);


        }

        if (winWidth > 767) {

            featuredProductsResize();

        }

    }, 500);


})

$(function() {

    windowWidth = checkWinWidth();

    if (Modernizr.svg == false) {
        var imgs = document.getElementsByTagName('img');
        var svgExtension = /.*\.svg$/
        var l = imgs.length;
        for(var i = 0; i < l; i++) {
            if(imgs[i].src.match(svgExtension)) {
                imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
                console.log(imgs[i].src);
            }
        }
    }

    // CHECK IF BROWSER IS IE9 AND RUN SCRIPT FOR PLACEHOLDERS
    if ($('html').hasClass('no-csstransitions')) {

        $('input,textarea').placeholder();

    }

    // RUN NAV TABS FUNCTIONS IF FOUND
    if ($('.nav-tabs').length)
        tabFunctions();


    // RUN PROD SLIDER IF FOUND
    if ($('#prod-slider').length) {

        productSlider();

    }


    // RUN BG IMAGE PLACER IF MAIN SLIDER FOUND
    if ($('.carousel.main').length) {
        var sliderItems = $('.carousel.main').find('.item img')
        placeBackgroundImage(sliderItems);
    }


    // IF PRODUCT EXIST, MAKE PROD IMAGE BG
    if ($('.products .product').length) {

        var products = $('.products').find('.product');
        productImage(products);

    }


    // IF PROMOTIONS PAGE INITIALIZE TIMER
    if ($('#clockdiv').length) {
        var deadline = '2016-12-31';
        initializeTimer('clockdiv', deadline);
        initializeIsotope();

    }


    // IF FEATURE IMAGE EXISTS, RESIZE 
    if ($('.feature-products').length) {
        featuredProductsResize();
    }


    // Sticky Nav Functions 
    stickyNav();


    // Mobile Slide Menu Intialize
    mobiNav();


    // Mega Menu Dropdown Functions
    megaMenuFunctions();


    // Search Input Listeners
    searchListener();

});


function featuredProductsResize() {

    var featured_products = $('.feature-products').find('.category a img');
    var iter = 0;

    featured_products.each(function() {

        var t = $(this);
        var magic_num = 0.526;
        var w = t.width();
        var h = w * magic_num;


        if (iter == 0) {
            var primaryParent = $(t).parents('.primary-image');
            $(primaryParent).css('max-height', h + 'px')
            iter++;
        } else if (iter == 1) {
            var secondaryParent = $(t).parents('.secondary-image');
            $(secondaryParent).css('max-height', h + 'px');
            iter++;
        } else if (iter == 2) {
            setTimeout(function() {

                $('.feature-products').css('opacity', '1');

            }, 600);

        }


        $(t).css({
            'max-height': h + 'px',
            'min-height': h + 'px'
        });


    })

}

function initializeIsotope() {

    $('.grid').isotope({
        layoutMode: 'masonry',
        itemSelector: '.promotion-item',
        masonry: {
            // options
            percentPosition: true,
            gutter: '.gutter-sizer',
            isAnimated: true
        }
    });

}

function getTimeRemaining(endtime) {

    var t = Date.parse(endtime) - Date.now();
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));

    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };

}

function initializeTimer(id, endtime) {
    var clock = document.getElementById(id);
    var daysSpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');


    function updateClock() {

        var t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days + 'd';
        hoursSpan.innerHTML = t.hours + 'h';
        minutesSpan.innerHTML = t.minutes + 'm';
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2) + 's';

        if (t.total <= 0) {
            clearInterval(timeinterval);
        }

    }

    updateClock(); // run function once at first to avoid delay
    var timeinterval = setInterval(updateClock, 1000);

}

function placeBackgroundImage(targetItems) {

    targetItems.each(function() {

        var t = $(this);
        var parent = t.parent();
        var imgSrc = t.attr('src');

        parent.css({
            background: 'url(' + imgSrc + ') no-repeat center'
        });

        t.hide();

    })

}

function checkWinWidth() {

    $('html,body').css('overflow', 'hidden');
    var winWidth = $(window).width();
    $('html,body').css('overflow-y', 'visible');

    return winWidth;

}

function productImage(productArray) {

    var prodRows = $('.products').find('.p-row');

    prodRows.each(function() {

        var t = $(this);
        var productDesc = t.find('.product-details');
        var maxHeight = 0;

        productDesc.each(function() {

            if ($('#productTabs').length)
                var tabContainer = $(this).parents('.tab-pane');

            if ($(this).is(":visible")) {
                var height = $(this).outerHeight();
            } else {
                tabContainer.css({

                    position: 'absolute',
                    left: '-99999px',
                    display: 'block'

                });

                var height = $(this).outerHeight();

                tabContainer.css({

                    position: 'relative',
                    left: '0',
                    display: ''

                });
            }

            if (height > maxHeight)
                maxHeight = height;

        });

        productDesc.css('height', maxHeight);

    })

}

function mobiNav() {

    var slideContainer = $('#mobi-slide');
    var navItemsParents = slideContainer.find('.side-nav > li');
    var navItems = slideContainer.find('.dropdown-trigger');
    var mobiToggle = $('#master-navigation').find('.mobi-trigger > button');
    var mobiSlideToggle = slideContainer.find('button.close');

    mobiToggle.click(function() {

        $('html').addClass('slide-open');

    });


    mobiSlideToggle.click(function() {

        $('html').removeClass('slide-open');

    })


    navItems.click(function(e) {

        e.preventDefault();
        var t = $(this);

        if (!navItemsParents.hasClass('open')) {
            t.parent().addClass('no-delay');

            setTimeout(function() {

                t.parent().removeClass('no-delay');

            }, 500)
        }


        if (t.parent().hasClass('open')) {
            t.parent().toggleClass('open');
        } else {

            navItemsParents.removeClass('open');
            t.parent().toggleClass('open');

            setTimeout(function(){

                $('html,body').animate({
                    scrollTop: t.offset().top - 30,
                },500);

            },501);

        }

    })

}

function stickyNav() {

    var mainNav = $('#master-navigation #navbar-alpha');
    var clonedNav = mainNav.clone(true,true);
    clonedNav.insertAfter(mainNav).addClass('sticky-nav');
    var mnHeight = $(mainNav).outerHeight();
    var offsetAnchor;
    var windowOffset = 0;
    var curPos = 0;

    if ($('.scrollAnchor').length) {
        offsetAnchor = $('.scrollAnchor').offset().top;
    } else {
        offsetAnchor = 400;
    }

    $(window).scroll(function() {

        windowOffset = $(window).scrollTop();

        $(clonedNav).find('.desktop-dropdown').attr('style', '');
        $(clonedNav).find('.desktop-dropdown').removeClass('visible');

        if (windowOffset > offsetAnchor) {

            // Check if Cur Position is More than window dist top
            if (curPos < windowOffset) {

                clonedNav.removeClass('stickified');

            } else {

            	clonedNav.addClass('transition');
            	clonedNav.addClass('stickified');

            }

        } else {


            if (windowOffset <= offsetAnchor) {

                if (clonedNav.hasClass('stickified')) {

                    clonedNav.removeClass('transition');
                    clonedNav.removeClass('stickified');

                }

            }

        }

        // Save Cur Scroll Position
        curPos = windowOffset;

    })

}

function colourModal(modal_id, items_container_class, colour_slider_id) {
    $('.' + items_container_class + ' .item:first').addClass('active');

    var colorModal = $('#' + modal_id);
    var colourItems = $('.' + items_container_class).find('.item');
    var colourItemsNav = $('.' + items_container_class).find('.carousel-indicators');

    colourItems.each(function(i, j) {
        colourItemsNav.append('<li data-target="#' + colour_slider_id + '" data-slide-to="' + (jQuery(j).data('slide-number') - 1) + '"></li>');
    });

    colourItems.appendTo($(colorModal).find('.carousel-inner'));
    colourItemsNav.insertBefore($(colorModal).find('.carousel-inner'));

    var slideItems = $(colorModal).find('.item');

    colourItemsNav.find('li:first-child').addClass('active');

    moveImg(slideItems);

    colorModal.on('show.bs.modal', function() {

        $('html, body').addClass('overflow-hide');

    }).on('hidden.bs.modal', function() {

        $('html, body').removeClass('overflow-hide');

    });

    setTimeout(function() {

       $('.col-md-6 > div[class^=prod-colours]').appendTo($('.col-md-6 > .slider-btns')).find('.slider-btns').css({

        'border-top': '0',
        'padding-top': '0'

       }); 

    }, 200);
    
}

function moveImg(carouselItems) {

    carouselItems.each(function() {

        var curImage = $(this).find('img');
        var curImageSrc = curImage.attr('src');

        $(this).css('background-image', 'url(' + curImageSrc + ')');
        curImage.css('visibility', 'hidden');

    })

}

function productSlider() {

    $('#prod-slider').carousel({
        interval: 5000
    });

    // Fetch Elements to add Listeners and Calcultions
    var thumbnailNav = $('[id^=carousel-selector-]');
    var thumbsContainer = $('.thumbs-container');
    var thumbsList = thumbsContainer.find('.thumbnails');
    var thumbnailItems = $('#slider-thumbs').find('.thumbnail');
    var thumbsContainerWidth = $('#slider-thumbs').width();
    var thumbs = $('#slider-thumbs').find('li');
    var sliderItems = $('#prod-slider').find('.item');
    var noItems = thumbnailItems.length;
    var left = 0;
    var thumbPad = parseInt($('#slider-thumbs').find('li:first').css('padding-right'));
    itemWidth = Math.round((thumbsContainerWidth / 3)) + thumbPad;
    var slideWidth = (noItems * itemWidth);
    var slidItems = 0;



    thumbs.each(function() {

        $(this).css({

            'width': (itemWidth) + 'px'

        });

    });

    // Place Images as BG images to scale correctly
    moveImg(thumbnailItems);
    //moveImg(sliderItems);

    //Handles the carousel thumbnails
    thumbnailNav.click(function(e) {

        e.preventDefault();

        var t = $(this);
        var id_selector = t.attr("id");
        var id = id_selector.substr(id_selector.length - 1);
        var id = parseInt(id) - 1;
        var curSlide = $('#prod-slider').find('.item.active');
        var curSlideIndex = $(curSlide).index();

        if ($('#slider-thumbs').find('.thumbs-container').css('left') !== '0px') {
            return;
        } else {

            if (curSlideIndex === id) {
                return;
            } else if (noItems === 1) {
                $('#prod-slider').carousel(id);
            } else if (noItems >= 2) {

                if ($('#' + id_selector).parent().is(':first-child')) {
                    $('.carousel-control.left').trigger('click');
                } else {
                    $('.carousel-control.right').trigger('click');
                }

            }

        }



    });


    thumbsContainer.css('width', slideWidth);

    thumbsContainer.css('left', left + 'px');


    // PRODUCT SLIDER ON SLIDE LISTENER
    $('#prod-slider').on('slide.bs.carousel', function(e) {

        var curSlide = $(this).find('.active');
        var curSlideIndex = curSlide.index();
        var nextSlide = $(e.relatedTarget);
        var nextSlideIndex = $(nextSlide).index();

        if (noItems >= 3) {

            var slideDirection = e.direction;

            lastThumb = thumbsContainer.find('.thumbnails > li').last();
            firstThumb = thumbsContainer.find('.thumbnails > li').first();



            if (slideDirection === 'right') {

                thumbsContainer.css('width', slideWidth + itemWidth);

                var clonedThumb = lastThumb.clone()
                clonedThumb.insertBefore(firstThumb);

                thumbsContainer.css('left', -itemWidth + 'px');

                thumbsContainer.animate({

                    'left': 0

                }, 600, function() {

                    lastThumb.insertBefore(firstThumb);
                    clonedThumb.remove();
                    thumbsContainer.css('width', slideWidth);

                });

            }



            if (slideDirection === 'left') {

                thumbsContainer.css('width', slideWidth + itemWidth);

                var clonedThumb = firstThumb.clone();
                clonedThumb.insertAfter(lastThumb);

                thumbsContainer.animate({

                    'left': -(itemWidth)

                }, 600, function() {

                    firstThumb.insertAfter(lastThumb);
                    clonedThumb.remove();
                    thumbsContainer.css('left', 0 + 'px');
                    thumbsContainer.css('width', slideWidth);

                });

            }


            thumbnailItems.removeClass('active');
            $(thumbnailItems[nextSlideIndex]).addClass('active');


        } else {

            if (noItems === 2) {

                $(thumbnailNav[nextSlideIndex]).addClass('active');
                $(thumbnailNav[curSlideIndex]).removeClass('active');

            }

        }


    });

}

var search_results_current_page = 0;

function loadSearchResults(search_identifier) {
    search_results_current_page += 9;
    var offset = 1;
    $.ajax({
        method: 'GET',
        url: base_path + "_ajax/fetch-search-results/" + search_identifier + '/P' + search_results_current_page,
        context: document,
        beforeSend: function() {
            $('.load-more').addClass('visible');
        }
    }).done(function(data) {
        // .p-row - Product Row
        data = $(data);

        var products = data.find('.product');

        if (products.length > 0)
            data.insertAfter($('.products .p-row').last());


        productImage(products);
        products.each(function() {
            var t = $(this);
            t.addClass('fadeInDown d' + offset);
            offset = offset + 1;
        })

        if (products.length < 9) {
            $('.load-more').html('All Products Loaded').css({
                background: '#EFEFEF',
                color: '#888',
                pointerEvents: 'none'
            });
        }

        $('.load-more').removeClass('visible');
    });
}

var loaded_products_current_offset = 0;

function loadProductRows(channel, category_id, orderby) {


    rotateInt = setInterval(function() {

        rotation += 5;
        $('.load-more .loading .ie9').rotate(rotation);

    }, 150);


    if ($('html').hasClass('no-csstransitions')) {

        rotateInt;

    }

	if (channel == 'mattresses')
		loaded_products_current_offset += 3;
	else
		loaded_products_current_offset += 9;
	
    var offset = 1;
    category_id = (category_id == '' ? 'all' : category_id);
    orderby = (orderby == '' ? 'none' : orderby);

    $.ajax({
        method: 'GET',
        url: base_path + "_ajax/fetch-product-rows/" + channel + '/' + category_id + '/' + orderby + '/' + loaded_products_current_offset,
        context: document,
        beforeSend: function() {
            $('.load-more').addClass('visible');
        }
    }).done(function(data) {

        // .p-row - Product Row

        data = $(data);

        var products = data.find('.product');

        if (products.length > 0)
            data.insertAfter($('.products .p-row').last());


        productImage(products);
        products.each(function() {

            var t = $(this);
            t.addClass('fadeInDown d' + offset);
            offset = offset + 1;

        })

		var limit_check = (channel == 'mattresses' ? 3 : 9);
        if (products.length < limit_check) {

            $('.load-more').html('All Products Loaded').css({

                background: '#EFEFEF',
                color: '#888',
                pointerEvents: 'none'

            });

        }

        $('.load-more').removeClass('visible');
        clearInterval(rotateInt);


    });
}

function searchListener() {

    var searchBtn = $('a.search');
    var mobileSearch = $('#mobile-search');
    var mobileClose = mobileSearch.find('.menu-close');
    var animLocker = false;

    $.each(searchBtn, function(index, val) {
    	 

        var searchForm = searchBtn.closest('#navbar-alpha');
        var i = 0;
        var t = $(this);
        var tSearchInput = t.parents('.nav-holder').find('.search-form');
    	
    	// needed to now work out the width, as there is a potential cart button to the right of the search button...
    	// the excitement...
    	var width = t.parents('.nav-holder').width() - jQuery('li a.nav-item-alpha.shopping-cart').width();
    	var right = 0 + jQuery('li a.nav-item-alpha.shopping-cart').outerWidth();

        t.click(function(e) {

            e.preventDefault();

            winWidth = checkWinWidth();

            if(winWidth > 992) {

                // We are desktop
                if (!tSearchInput.hasClass('visible')) {

                    t.addClass('active');
                    tSearchInput.addClass('visible');
                    tSearchInput.css('margin-right', right + 'px');
                    tSearchInput.css('width', width + 'px');

                    setTimeout(function() {
                        tSearchInput.find('.search-input').focus();
                    }, 351);

                } else {

                    t.removeClass('active');
                    tSearchInput.removeClass('visible');
                    tSearchInput.css('width', '0px');

                }

            } else {

                // We are mobile here

                if(!animLocker) {

                    animLocker = true;
                    if(!mobileSearch.is(':visible')) {

                        $('body').toggleClass('overflow-hide',true);
                        mobileSearch.fadeIn('400',function(){

                            animLocker = false;

                        });

                    }

                }


            }

            

        });


        mobileClose.click(function(e){

            e.preventDefault();
            animLocker = true;
            $('body').toggleClass('overflow-hide',false);
            mobileSearch.fadeOut('400',function(){

                animLocker = false;

            });


        })

        tSearchInput.find('.search-input').focusout(function() {

            setTimeout(function() {

                t.removeClass('active');
                tSearchInput.removeClass('visible');
    			tSearchInput.css('width', '0px');

            }, 400)

        })


    });

    

}

function megaMenuFunctions() {


    var listeners = $('body').find('#master-navigation .dropdown-toggle');
    var desktopNavLists = $('#master-navigation').find('.dropdown');
    var megaMenu = $('#desktop-modal');
    var mobiMenu = $('#mobi-slide');
    var menuOpen = $('#master-navigation').find('.menu-toggle');
    var menuClose = $('#desktop-modal').find('.menu-close');
    var hoverOutTimer;
    var target;


    listeners.click(function(e) {

        e.preventDefault();
        clearTimeout(hoverOutTimer);
        $('#master-navigation').toggleClass('open',true);

        // Need to fetch the parent here so we only target one dropdown
        var t = $(this);
        var parent = $(this).parents('#navbar-alpha:first');
        target = parent.find($(t).attr('data-target'));
        desktopNavLists.not(target).removeClass('in');



        // check if item clicked on is open
        if(t.hasClass('nav-open')) {

            // We want to close
            t.toggleClass('nav-open',false);

            target.animate({

                opacity: 0,
                top: '-60px'

            }, 200, function() {

                target.css({

                    visibility:'hidden',

                });

                $('#master-navigation').toggleClass('open',false);

            });

        } else {

            // Open plox
            t.toggleClass('nav-open',true);


            // Make sure all the dropdowns are hidden before we go ahead and animate
            desktopNavLists.not(target).animate({

                opacity: 0,
                top: '-60px'

            }, 200, function() {

                desktopNavLists.not(target).css({

                    visibility:'hidden',

                });

                target.css('visibility','visible');

            });


            if(target.css('visibility') == 'hidden') {

                target.css({

                    visibility:'visible',

                }).toggleClass('in',true).animate({

                    opacity: 1,
                    top: '90px'

                }, 400, "swing");


            } 

        }


        listeners.not(t).toggleClass('nav-open',false);


    });

    var locker = false;
    menuOpen.on('click',function(e){

        e.preventDefault();

        // Check for mobile rez
        if(windowWidth < 992) {

            // Slide to be used for mobile
            if(!locker) {

                locker = true;
                if(mobiMenu.is(':visible')) {

                    mobiMenu.slideUp(300,function(){
                        locker = false;
                    });

                } else {

                    mobiMenu.slideDown(300,function(){
                        locker = false;
                    });

                }

                $('#master-navigation').toggleClass('open');

            }


        } else {


            // Fade out for desktop
            if(!locker) {

                locker = true;
                if(megaMenu.is(':visible')) {

                    megaMenu.fadeOut(300,function(){
                        locker = false;
                    });

                } else {

                    megaMenu.fadeIn(300,function(){
                        locker = false;
                    });

                }

            }

        }

        if(menuOpen.hasClass('open')) {

            // We need to close here
            menuOpen.toggleClass('open',false);

        } else {

            // We open here
            menuOpen.toggleClass('open',true);

        }


    });

    menuClose.on('click',function(e){

        e.preventDefault();

        if(!locker) {

            locker = true;
            megaMenu.fadeOut(300,function(){
                locker = false;
            });

            menuOpen.toggleClass('open',false);            

        }

    });


    desktopNavLists.on('mouseleave', function() {

        clearTimeout(hoverOutTimer);
        hoverOutTimer = setTimeout(function(){

            desktopNavLists.removeClass('in');

            desktopNavLists.animate({

                opacity: 0,
                top: '-60px'

            }, 200, function() {

                desktopNavLists.css({

                    visibility:'hidden',

                })

            });

            $('#master-navigation').toggleClass('open',false);
            listeners.toggleClass('nav-open',false);

        },700);

    }).on('mouseenter',function(){
        clearTimeout(hoverOutTimer);
    });

}

function tabFunctions() {

    $('.nav-tabs a').click(function(e) {
        e.preventDefault();

        if ($(this).attr('data-tab')) {

            $(this).parents('.nav').removeClass('tab1 tab2 tab3');

            var activeTab = $(this).attr('data-tab');

            $(this).parents('.nav').addClass(activeTab);

        }

        $(this).tab('show');

    })

}


var rotation = 0;

jQuery.fn.rotate = function(degrees) {
    $(this).css({
        '-webkit-transform': 'rotate(' + degrees + 'deg)',
        '-moz-transform': 'rotate(' + degrees + 'deg)',
        '-ms-transform': 'rotate(' + degrees + 'deg)',
        'transform': 'rotate(' + degrees + 'deg)'
    });
    return $(this);
};
