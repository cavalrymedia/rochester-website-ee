
	var member_created = false;

	$(function(){
		
		populateFreeform();
		
		// skip this, in case the member was already created...
		if (member_created)
			createEntry();

		$form = $('.register-form');

		$form.submit(function(event) {
			
			event.preventDefault();
			
			var form = jQuery(this);
			form.find('button[type=submit]').text('Please wait...').prop('disabled','disabled');

			// Fetch values 
			action = $(this).prop('action');
			params = $(this).serialize();

			$.post(action, params, function(data) {

				data = data + '';
				if (data.indexOf('<title>Error</title>') >= 0)
				{
					alert('There was an error registering your account, please try again.');
					form.find('button[type=submit]').text('Register').prop('disabled',false);
				}
				else
				{
					member_created = true;
					createEntry();
				}

			}, 'html');

		});

	});

	function createEntry() {

		$entryForm = $('#create-entry');
		
		$.post($entryForm.prop('action'), $entryForm.serialize(), function(data) {
			
			if (data.success == true)
				window.location.href = data['return_url'];
			else
			{
				alert('There was an error registering your account, please try again.');
				console.log(data);
			}

		}, 'json');

	}

	function populateFreeform() {

		registerForm = $('.register-form');

		// Fetch values from register form
		// Populate freeform entry
		$(registerForm).find('.form-control').on('blur',function(){

			value = $(this).val();
			target = $(this).data('field');

			$('input[name=' + target + ']').prop('value',value).val(value);

		});

	};