/*
 *	(c)2016 Cavalry Media
 *	http://www.hellocavalry.com
 *	Last updated: 14 September 2016
 * 
 *	This script requires jQuery and ImagesLoaded by desandro:
 *	https://jquery.com
 *	http://imagesloaded.desandro.com/
 *
 *	You also need to add the following 2 elements to your BODY tag for this
 *	script to utilise and work correctly. Without these elements, the script will NOT work:
 *	
 *	<div id="cavalry-preloader-curtain"></div>
 *	<div id="cavalry-preloader-counter"></div>
 *
 */

var rotateInt;

(function() {

    var curtain = jQuery('#cavalry-preloader-curtain');
    var counter = jQuery('#cavalry-preloader-counter');

    if (curtain.length > 0 && counter.length > 0) {


    	if ($('html').hasClass('no-csstransitions')) {

    	    rotateInt = setInterval(function() {

    	        rotation += 5;
    	        $('.preloader-contents img').rotate(rotation);

    	    }, 100);

    	}


        jQuery('body')
            .imagesLoaded({
                background: true
            })
            .always(function(instance) {

                setTimeout(function() {
                    curtain.fadeOut(650);
                    counter.fadeOut(650, function(){

                    	clearInterval(rotateInt);
						
						if (typeof cavalryPreloaderFinished == 'function')
							cavalryPreloaderFinished();

                    });
                }, 200);

                

            })
            .done(function(instance) {})
            .fail(function() {})
            .progress(function(instance, image) {

                var perc = Math.round(instance.progressedCount / instance.images.length * 100);
                counter.text(perc + '%');

            });
    }

})();


