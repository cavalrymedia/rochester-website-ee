(function() {
    var footerMenu = $('#icon-menu-footer');
    var footerMenuContainer = footerMenu.find('.row');
    var mobiMenuOpen = footerMenu.find('.expand-mobile');
    var htmlEl = $('html');
    var canBeClicked = false;

    // $('.carousel-showmanymoveone .item').each(function() {
    //     var itemToClone = $(this);

    //     for (var i = 1; i < 3; i++) {
    //         itemToClone = itemToClone.next();
    //         // wrap around if at end of item collection
    //         if (!itemToClone.length) {
    //             itemToClone = $(this).siblings(':first');
    //         }
    //         // grab item, clone, add marker class, add to collection
    //         itemToClone.children(':first-child').clone()
    //             .addClass("cloneditem-" + (i))
    //             .appendTo($(this));
    //     }
    // });

    mobiMenuOpen.on('click', function() {
        if (canBeClicked) {
            footerMenu.toggleClass('open');
            htmlEl.toggleClass('overflow-hidden');
        }
    });

    if (footerMenu.length) {
        $(window).scroll(function() {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 300) {
                footerMenu.addClass('visible');
                canBeClicked = true;
            }
            if ($(window).scrollTop() + $(window).height() < $(document).height() - 300) {
                footerMenu.removeClass('visible');
                canBeClicked = false;
            }
        });
        $(window).resize(function() {
            if ($(window).width() > 500) {
                htmlEl.removeClass('overflow-hidden');
                footerMenu.removeClass('open');
            }
        })
    }


    $('#icon-menu-footer').find('.square > a').each(function(i,e){

        t = $(this);
        t.click(function(e){

            var tab = $(this).prop('hash');

            target = $('.nav-tabs');
            distTop = $(target).offset().top - 100;

            e.preventDefault();
            $('html, body').animate({
                scrollTop: distTop
            }, 2000, function(){

                $(target).find('a[href="' + tab + '"]').click();

            });

        })

    })


}());