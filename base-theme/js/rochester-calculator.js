
	/* calculator fun... */

	function calculateFinanceAmounts(p_amount, p_months)
	{
		// to supply...
		var months = parseInt(p_months);
		var initial = parseFloat((p_amount + '').replace(/[^0-9\.]+/gi, ''));
		
		if (months > 24 && initial <= 4000)
			return {payment:"0.00", deposit:"0.00", total:"0.00"};
		
		// static...
		var interest_rate_perc = 23.75;
		var deposit_perc = 10;
		var tax_perc = 14;
		var min_initiation_fee = 165.00;
		var initiation_fee_perc = 10;
		var max_initiation_fee = 1050;
		var basic_insurance_perc = 8;
		var life_insurance_perc = 5.5;
		var service_fee_amount = 61.56;
		
		// calculated...
		var pmt_monthly_interest = interest_rate_perc / 100 / 12;
		var pmt_const = pmt_monthly_interest / (1 - Math.pow((1 + pmt_monthly_interest), (-1 * months)));
		var initial_net = initial / (1 + (tax_perc / 100));
		var deposit_amount = initial * deposit_perc / 100;
		var deferred_amount = initial - deposit_amount;
		var deferred_amount_net = initial_net - deposit_amount;
		
		// calculate the initiation fee...
		var initiation_fee_net = 0.00;
		if (deferred_amount_net > 1000)
		{
			if ((min_initiation_fee + ((deferred_amount_net - 1000) * (initiation_fee_perc / 100))) > max_initiation_fee)
				initiation_fee_net = max_initiation_fee;
			else
				initiation_fee_net = (min_initiation_fee + ((deferred_amount_net - 1000) * (initiation_fee_perc / 100)));
		}
		else
			initiation_fee_net = min_initiation_fee;
		var initiation_fee = initiation_fee_net * (1 + (tax_perc / 100));
		
		var principle_debt = deferred_amount + initiation_fee;
		var installment = pmt_const * principle_debt;
		var total_installment = installment * months;
		var financial_charges = total_installment - principle_debt;
		
		// calculate the insurances...
		var total_basic_insurance = 0.00;
		var total_life_insurance = 0.00;
		var principle_debt_clone = principle_debt;

		while (principle_debt_clone > 0)
		{
			total_basic_insurance += (principle_debt_clone * (basic_insurance_perc / 100 / 12)) * (1 + (tax_perc / 100));
			total_life_insurance += (principle_debt_clone * (life_insurance_perc / 100 / 12));
			principle_debt_clone += (principle_debt_clone * interest_rate_perc / 100 / 12);
			principle_debt_clone -= installment;
		}
		
		var service_fee = service_fee_amount * months;
		var total_deal_amount = total_installment + total_basic_insurance + total_life_insurance + service_fee;
		var monthly_payment = total_deal_amount / months;
		
		// update to return no cents...
		total_deal_amount = monthly_payment * months;
		
		/*console.log('pmt_monthly_interest', pmt_monthly_interest);
		console.log('pmt_const', pmt_const);
		console.log('initial_net', initial_net);
		console.log('deposit_amount', deposit_amount);
		console.log('deferred_amount', deferred_amount);
		console.log('deferred_amount_net', deferred_amount_net);
		console.log('initiation_fee_net', initiation_fee_net);
		console.log('initiation_fee', initiation_fee);
		console.log('principle_debt', principle_debt);
		console.log('installment', installment);
		console.log('total_installment', total_installment);
		console.log('financial_charges', financial_charges);
		console.log('total_basic_insurance', total_basic_insurance);
		console.log('total_life_insurance', total_life_insurance);
		console.log('principle_debt_clone', principle_debt_clone);
		console.log('service_fee', service_fee);
		console.log('monthly_payment', monthly_payment);
		console.log('total_deal_amount', total_deal_amount);*/
		
		//return {payment:monthly_payment.toFixed(2), deposit:deposit_amount.toFixed(2), total:total_deal_amount.toFixed(2)};
		return {payment:monthly_payment.toFixed(0), deposit:deposit_amount.toFixed(0), total:total_deal_amount.toFixed(0)};
	}
	
	
	
	
	/* jQuery plugin... */

	jQuery.fn.rochesterCalculator = function(p_amount){
	
		var self = this;
		
		this.initial_amount = parseFloat(p_amount);
		this.terms = [12, 24, 36, 48];
		
		this.terms_select = jQuery('<select class="calculator-terms"></select>');
		self.terms_select.append('<option value=""></option>');
		jQuery.each(self.terms, function(i, v){
			self.terms_select.append('<option value="'+ v +'">'+ v +' months</option>');
		});
		
		this.status_container = jQuery('<div class="calculator-status"></div>');
		
		this.updateSelection = function(){
			var terms = self.terms_select.val();
			if (terms != '')
			{
				terms = parseInt(terms);
				var items = calculateFinanceAmounts(p_amount, terms);
				
				if (items.payment != "0.00")
					self.status_container.html('<span>R' + items.payment + 'p/m (deposit R'+ items.deposit +')</span><span>Total payment of R'+ items.total + '</span>');
				else
					self.status_container.html('<span>No available finance options available.</span>');
			}
			else
				self.status_container.html('');
		};
		
		jQuery(this)
			.append('<span>Months:</span>')
			.append(self.terms_select)
			.append(self.status_container);
			
		self.terms_select.bind('change', self.updateSelection);
	
	};
	