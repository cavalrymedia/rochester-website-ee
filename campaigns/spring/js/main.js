/* =Main INIT Functions
-------------------------------------------------------------- */
function initializeRochester() {

	"use strict";

	//PARALLAX EFFECTS
	jQuery('.parallax-bg1').parallax("50%", 0.5);
	jQuery('.parallax-bg2').parallax("50%", 0.9);
	jQuery('.parallax-bg3').parallax("50%", 0.7);
	
	//LOCAL SCROLL ANIMATION
	$(function() {
	  $('a.scroll[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	
	
	//ADD TO WISHLIST AJAX SUBMISSION
	    jQuery('#add_to_wishlist').click(function() {
	
	        var url = $(this.form).attr('action');
	        var data = $(this.form).serialize();
	        $.post(url, data, function() {
	            $('#wish-list-modal').load('/index.php/wish-list/_cart');
				$('#wish-list-modal').modal('show');
			
	        });

	        return false;

	    });
	   
	   	jQuery('#section-nav a, #scroll-nav a').click(function(){
		    $('html, body').animate({
		        scrollTop: $( $.attr(this, 'href') ).offset().top
		    }, 500);
		    return false;
		});

	//RESPONSIVE HEADINGS
	jQuery("h1,h2").fitText(1.5, { minFontSize: '20px', maxFontSize: '36px' });
	jQuery(".news-block h2").fitText(1.5, { minFontSize: '20px', maxFontSize: '24px' });
	
	//HERO DIMENSTION AND CENTER
	(function() {
	    function heroInit(){
	       var hero = jQuery('.flexslider'),
				ww = jQuery(window).width(),
				wh = jQuery(window).height(),
				heroHeight = wh;

			hero.css({
				height: heroHeight+"px",
			});

			var heroContent = jQuery('.flexslider .container'),
				contentHeight = heroContent.height(),
				parentHeight = hero.height(),
				topMargin = (parentHeight - contentHeight) / 3;

			heroContent.css({
				"margin-top" : topMargin+"px"
			});
	    }

	    jQuery(window).on("resize", heroInit);
	    jQuery(document).on("ready", heroInit);
	})();
	
};

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){

	initializeRochester();
	
});
/* END ------------------------------------------------------- */