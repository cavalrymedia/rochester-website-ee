<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cavalry_autoresponders
{
	// to decrease development time and verbose code, these configuration variables were introduced...
	// please ensure these are accurate before utilising this plugin...
	
	// freeform variables...
	private $FREEFORM_APPLICATIONS_FORM_ID					= 21;
	private $FREEFORM_ADMIN_NOTIFICATION_1_TEMPLATE_ID		= 17;
	private $FREEFORM_ADMIN_NOTIFICATION_2_TEMPLATE_ID		= 19;
	private $FREEFORM_USER_24HOURS_NOTIFICATION_TEMPLATE_ID	= 18;
	private $FREEFORM_APPLICATIONS_FIELD_ID_MAPPING			= array(
		'name'					=> 5,
		'email'					=> 3,
		'contact_number'		=> 6,
		'customer_sentiment'	=> 108,
		'sales_person'			=> 110,
		'sales_order_number'	=> 109,
		'other_party'			=> 112,
		'related_branch'		=> 113,
		'message'				=> 9,
		'flag_followup'			=> 130,
		'timestamp'				=> 129
	);
	
	// branches variables...
	private $BRANCHES_CHANNEL_ID				= 4;
	private $CONTACT_POINTS_GRID_FIELD_ID		= 5;
	private $CONTACT_POINTS_GRID_FIELD_VALUE_ID	= 1;
	private $BRANCHES_CHANNEL_FIELD_ID_MAPPING	= array(
		'branch_email_contact'	=> 7
	);
	
	public function __construct()
    {
		ee()->load->library('email');
		ee()->load->helper('text');
    }
	
	public function send_initial_admin_emails()
	{
		// fetch submissions that haven't been processed yet...
		$results = ee()->db->select()
			->from('freeform_form_entries_'. $this->FREEFORM_APPLICATIONS_FORM_ID)
			->where(array(
				'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['flag_followup']	=> '0'
			))
			->get();
		
		if ($results->num_rows() == 0)
			return false;
		
		// get the initial admin e-mail template...
		$template = ee()->db->select()
			->from('freeform_notification_templates')
			->where(array(
				'notification_id'	=> $this->FREEFORM_ADMIN_NOTIFICATION_1_TEMPLATE_ID
			))
			->get();
			
		if ($template->num_rows() == 0)
			return false;
		
		$send_errors = array();
		$template = $template->result_array()[0];
		
		// mail the applicable recipients...
		ee()->email->wordwrap = true;
		ee()->email->mailtype = 'html';
		
		foreach ($results->result_array() as $row)
		{
			// format the html body...
			$html_body = $template['template_data'];
			foreach ($this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING as $field_name => $field_id)
				$html_body = str_replace('{'. $field_name .'}', $row['form_field_'.$field_id], $html_body);
			$html_body = str_replace('{freeform_entry_id}', $row['entry_id'], $html_body);
			
			// format the subject...
			$subject = $template['email_subject'];
			foreach ($this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING as $field_name => $field_id)
				$subject = str_replace('{'. $field_name .'}', $row['form_field_'.$field_id], $subject);
			$subject = str_replace('{freeform_entry_id}', $row['entry_id'], $subject);
			
			// get the entry ID for this branch...
			$branch_entry_id = ee()->db->select('entry_id')
				->from('channel_titles')
				->where(array(
					'channel_id'	=> $this->BRANCHES_CHANNEL_ID,
					'title'			=> $row['form_field_'.$this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['related_branch']]
				))
				->get();
			if ($branch_entry_id->num_rows() == 0)
				$branch_entry_id = 0;
			else
			{
				$branch_entry_id = $branch_entry_id->result_array()[0];
				$branch_entry_id = intval($branch_entry_id['entry_id']);
			}
			
			// format the TO...
			$to = array();
			$branch_email = ee()->db->select('field_id_' . $this->BRANCHES_CHANNEL_FIELD_ID_MAPPING['branch_email_contact'])
				->from('channel_data')
				->where(array(
					'entry_id'	=> $branch_entry_id
				))
				->get();
			if ($branch_email->num_rows() > 0)
			{
				$branch_email = $branch_email->result_array()[0];
				$to[] = $branch_email['field_id_' . $this->BRANCHES_CHANNEL_FIELD_ID_MAPPING['branch_email_contact']];
			}
			
			// format the CC's...
			$cc = array();
			$cc[] = 'rozell@rochester.co.za';
			$cc_addresses = ee()->db->select()
				->from('channel_grid_field_' . $this->CONTACT_POINTS_GRID_FIELD_ID)
				->where(array(
					'entry_id'	=> $branch_entry_id
				))
				->get();
			foreach ($cc_addresses->result_array() as $cc_row)
			{
				$value = trim($cc_row['col_id_' . $this->CONTACT_POINTS_GRID_FIELD_VALUE_ID]);
				if (strstr($value, '@') !== false)
					$cc[] = $value;
			}
			
			ee()->email->from($template['from_email'], $template['from_name']);
			//ee()->email->to('chris@hellocavalry.com');
			ee()->email->to($to);
			ee()->email->cc($cc);
			ee()->email->subject($subject);
			ee()->email->message($html_body);
			//ee()->email->reply_to(email, name);
			ee()->email->bcc(array('greg@hellocavalry.com', 'chris@hellocavalry.com'));
			
			if ( ! ee()->email->send())
			{
				// send failed, data was not cleared...
				$send_errors[] = ee()->email->print_debugger();
				ee()->email->clear();
				
				// update the entry follow-up flag and timestamp...
				ee()->db->update(
					'freeform_form_entries_' . $this->FREEFORM_APPLICATIONS_FORM_ID,
					array(
						'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['flag_followup']	=> '5', // error...
						'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['timestamp']		=> time(),
					),
					array(
						'entry_id'	=> $row['entry_id']
					)
				);
			}
			else
			{
				// update the entry follow-up flag and timestamp...
				ee()->db->update(
					'freeform_form_entries_' . $this->FREEFORM_APPLICATIONS_FORM_ID,
					array(
						'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['flag_followup']	=> '1', // first admin e-mail sent...
						'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['timestamp']		=> time(),
					),
					array(
						'entry_id'	=> $row['entry_id']
					)
				);
			}
		}
	}
	
	public function send_customer_autoresponders()
	{
		// check to see if we're sending these off during business hours...
		if (date('w') == '0' || date('w') == '6' || date('G') < '9' || date('G') > '16')
			return true;
		
		// fetch submissions that haven't been processed yet...
		$twenty_four_hours = 60 * 60 * 24;
		$twenty_four_hours = 0; // temporary override...
		
		$results = ee()->db->select()
			->from('freeform_form_entries_'. $this->FREEFORM_APPLICATIONS_FORM_ID)
			->where(array(
				'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['flag_followup']	=> '1'
			))
			->where('form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['timestamp'] . ' <= ', (time() - $twenty_four_hours))
			->get();
			
		if ($results->num_rows() == 0)
			return false;
		
		// get the initial admin e-mail template...
		$template = ee()->db->select()
			->from('freeform_notification_templates')
			->where(array(
				'notification_id'	=> $this->FREEFORM_USER_24HOURS_NOTIFICATION_TEMPLATE_ID
			))
			->get();
			
		if ($template->num_rows() == 0)
			return false;
		
		$send_errors = array();
		$template = $template->result_array()[0];
		
		// mail the applicable recipients...
		ee()->email->wordwrap = true;
		ee()->email->mailtype = 'html';
		
		foreach ($results->result_array() as $row)
		{
			// format the html body...
			$html_body = $template['template_data'];
			foreach ($this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING as $field_name => $field_id)
				$html_body = str_replace('{'. $field_name .'}', $row['form_field_'.$field_id], $html_body);
			$html_body = str_replace('{freeform_entry_id}', $row['entry_id'], $html_body);
			
			// format the subject...
			$subject = $template['email_subject'];
			foreach ($this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING as $field_name => $field_id)
				$subject = str_replace('{'. $field_name .'}', $row['form_field_'.$field_id], $subject);
			$subject = str_replace('{freeform_entry_id}', $row['entry_id'], $subject);
			
			ee()->email->from($template['from_email'], $template['from_name']);
			ee()->email->to($row['form_field_'.$this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['email']], $row['form_field_'.$this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['name']]);
			//ee()->email->cc($cc);
			ee()->email->subject($subject);
			ee()->email->message($html_body);
			//ee()->email->reply_to(email, name);
			ee()->email->bcc(array('greg@hellocavalry.com', 'chris@hellocavalry.com'));
			
			if ( ! ee()->email->send())
			{
				// send failed, data was not cleared...
				$send_errors[] = ee()->email->print_debugger();
				ee()->email->clear();
				
				// update the entry follow-up flag and timestamp...
				ee()->db->update(
					'freeform_form_entries_' . $this->FREEFORM_APPLICATIONS_FORM_ID,
					array(
						'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['flag_followup']	=> '6', // error 24 hour email...
						'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['timestamp']		=> time(),
					),
					array(
						'entry_id'	=> $row['entry_id']
					)
				);
			}
			else
			{
				// update the entry follow-up flag and timestamp...
				ee()->db->update(
					'freeform_form_entries_' . $this->FREEFORM_APPLICATIONS_FORM_ID,
					array(
						'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['flag_followup']	=> '2', // 24 hour follow-up sent...
						'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['timestamp']		=> time(),
					),
					array(
						'entry_id'	=> $row['entry_id']
					)
				);
			}
		}
	}
	
	public function escalate_unsatisfied_customer_response()
	{
		$entry_id = ee()->TMPL->fetch_param('entry_id');
		
		// fetch the submission...
		$result = ee()->db->select()
			->from('freeform_form_entries_'. $this->FREEFORM_APPLICATIONS_FORM_ID)
			->where(array(
				'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['flag_followup']	=> '2',
				'entry_id'	=> $entry_id
			))
			->get();
		
		if ($result->num_rows() == 0)
			return false;
		
		// get the initial admin e-mail template...
		$template = ee()->db->select()
			->from('freeform_notification_templates')
			->where(array(
				'notification_id'	=> $this->FREEFORM_ADMIN_NOTIFICATION_2_TEMPLATE_ID
			))
			->get();
			
		if ($template->num_rows() == 0)
			return false;
		
		$send_errors = array();
		$template = $template->result_array()[0];
		
		// mail the applicable recipients...
		ee()->email->wordwrap = true;
		ee()->email->mailtype = 'html';
		
		$result = $result->result_array()[0];
		
		// format the html body...
		$html_body = $template['template_data'];
		foreach ($this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING as $field_name => $field_id)
			$html_body = str_replace('{'. $field_name .'}', $result['form_field_'.$field_id], $html_body);
		$html_body = str_replace('{freeform_entry_id}', $result['entry_id'], $html_body);
		
		// format the subject...
		$subject = $template['email_subject'];
		foreach ($this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING as $field_name => $field_id)
			$subject = str_replace('{'. $field_name .'}', $result['form_field_'.$field_id], $subject);
		$subject = str_replace('{freeform_entry_id}', $result['entry_id'], $subject);
		
		// get the entry ID for this branch...
		$branch_entry_id = ee()->db->select('entry_id')
			->from('channel_titles')
			->where(array(
				'channel_id'	=> $this->BRANCHES_CHANNEL_ID,
				'title'			=> $result['form_field_'.$this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['related_branch']]
			))
			->get();
		if ($branch_entry_id->num_rows() == 0)
			$branch_entry_id = 0;
		else
		{
			$branch_entry_id = $branch_entry_id->result_array()[0];
			$branch_entry_id = intval($branch_entry_id['entry_id']);
		}
		
		// format the TO...
		$to = array();
		$branch_email = ee()->db->select('field_id_' . $this->BRANCHES_CHANNEL_FIELD_ID_MAPPING['branch_email_contact'])
			->from('channel_data')
			->where(array(
				'entry_id'	=> $branch_entry_id
			))
			->get();
		if ($branch_email->num_rows() > 0)
		{
			$branch_email = $branch_email->result_array()[0];
			$to[] = $branch_email['field_id_' . $this->BRANCHES_CHANNEL_FIELD_ID_MAPPING['branch_email_contact']];
		}
		
		// format the CC's...
		$cc = array();
		$cc[] = 'procurement@rochester.co.za';
		$cc[] = 'francois@rochester.co.za';
		/*$cc_addresses = ee()->db->select()
			->from('channel_grid_field_' . $this->CONTACT_POINTS_GRID_FIELD_ID)
			->where(array(
				'entry_id'	=> $branch_entry_id
			))
			->get();
		foreach ($cc_addresses->result_array() as $cc_row)
		{
			$value = trim($cc_row['col_id_' . $this->CONTACT_POINTS_GRID_FIELD_VALUE_ID]);
			if (strstr($value, '@') !== false)
				$cc[] = $value;
		}*/
		
		ee()->email->from($template['from_email'], $template['from_name']);
		//ee()->email->to('chris@hellocavalry.com');
		ee()->email->to($to);
		ee()->email->cc($cc);
		ee()->email->subject($subject);
		ee()->email->message($html_body);
		//ee()->email->reply_to(email, name);
		ee()->email->bcc(array('greg@hellocavalry.com', 'chris@hellocavalry.com'));
		
		if ( ! ee()->email->send())
		{
			// send failed, data was not cleared...
			$send_errors[] = ee()->email->print_debugger();
			ee()->email->clear();
			
			// update the entry follow-up flag and timestamp...
			ee()->db->update(
				'freeform_form_entries_' . $this->FREEFORM_APPLICATIONS_FORM_ID,
				array(
					'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['flag_followup']	=> '7', // admin e-mail 2 error...
					'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['timestamp']		=> time(),
				),
				array(
					'entry_id'	=> $result['entry_id']
				)
			);
		}
		else
		{
			// update the entry follow-up flag and timestamp...
			ee()->db->update(
				'freeform_form_entries_' . $this->FREEFORM_APPLICATIONS_FORM_ID,
				array(
					'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['flag_followup']	=> '3', // second admin e-mail sent...
					'form_field_' . $this->FREEFORM_APPLICATIONS_FIELD_ID_MAPPING['timestamp']		=> time(),
				),
				array(
					'entry_id'	=> $result['entry_id']
				)
			);
		}
	}
}