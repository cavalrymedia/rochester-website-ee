<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Cavalry_email_notification_ext {

    var $name       = 'Cavalry Email notification';
    var $version        = '1.0';
    var $description    = 'Email  notifications';
    var $settings_exist = 'y';
    var $docs_url       = ''; // 'https://ellislab.com/expressionengine/user-guide/';

    var $settings       = array();

    /**
     * Constructor
     *
     * @param   mixed   Settings array or empty string if none exist.
     */
    function __construct($settings = '')
    {
        $this->settings = $settings;
        ee()->load->library('email');
        ee()->load->helper('text');
    }

    function activate_extension()
    {
        $this->settings = array();


        $data = array(
            'class'     => __CLASS__,
            'method'    => 'sendEmail',
            'hook'      => 'freeform_module_insert_end',
            'settings'  => serialize($this->settings),
            'priority'  => 10,
            'version'   => $this->version,
            'enabled'   => 'y'
        );

        ee()->db->insert('extensions', $data);
    }

    function update_extension($current = '')
    {

        return TRUE;

    }

    function disable_extension()
    {
        ee()->db->where('class', __CLASS__);
        ee()->db->delete('extensions');
    }

    function sendEmail($fields, $entry_id)
    {
		$entry_id = intval($entry_id);
		
		ee()->load->library('email');
		ee()->load->helper('text');
		
		
		
		// all of this to get the e-mail address of the author...
		
		$results = ee()->db->select()
			->from('freeform_form_entries_30') // pre-applications freeform...
			->where(array('entry_id' => $entry_id))
			->get();
		
		if ($results->num_rows() == 0)
			return false;
		
		$freeform_entry = current($results->result_array());
		$author_id = intval($freeform_entry['author_id']);
		
		
		
		$results = ee()->db->select()
			->from('members') // pre-applications freeform...
			->where(array('member_id' => $author_id))
			->get();
			
		if ($results->num_rows() == 0)
			return false;
		
		$member_entry = current($results->result_array());
		$member_email = $member_entry['email'];
		
		
			
        ee()->email->wordwrap = true;
        ee()->email->mailtype = 'html';
        ee()->email->from('website@rochester.co.za', 'Rochester Furniture');
        ee()->email->to($member_email);
		ee()->email->bcc('hellocavalry@gmail.com');
		
		/*if ($entry_id == 34)
		{
			echo $fields['consumer_application_status'];
			echo '<br />';
			echo $member_email;
			
			$email_subject = 'Rochester Furniture - Finance Application Approved';
			
			ee()->email->subject($email_subject);
            ee()->email->message($email_msg);
            ee()->email->Send();
			exit();
		}*/

        if ($fields['consumer_application_status']=='NEW') {
            //send new status email here
            $email_msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

                            <head>
                                <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                                <title>Rochester Finance Application</title>
                                <!--general stylesheet-->
                                <style type="text/css">
                                body {
                                    background-color: #f6f3e4;
                                }
                                
                                p {
                                    padding: 0;
                                    margin: 0;
                                }
                                
                                h1,
                                h2,
                                h3,
                                h4,
                                p,
                                li,
                                a,
                                th {
                                    font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
                                }
                                
                                td {
                                    vertical-align: top;
                                }
                                
                                ul,
                                ol {
                                    margin: 0;
                                    padding: 0;
                                }
                                
                                .heading {
                                    border-radius: 3px;
                                    -webkit-border-radius: 3px;
                                    -moz-border-radius: 3px;
                                    -khtml-border-radius: 3px;
                                    -icab-border-radius: 3px;
                                }
                                </style>
                            </head>

                            <body marginheight="0" topmargin="0" marginwidth="0" leftmargin="0" background="#f6f3e4" style="margin: 0px; background-color: #f6f3e4;" bgcolor="#f6f3e4">
                                <table cellspacing="0" border="0" cellpadding="0" width="100%" style="margin:40px 0">
                                    <tbody>
                                        <tr valign="top">
                                            <td>
                                                <!--container-->
                                                <table cellspacing="0" cellpadding="0" border="0" align="center" width="750" bgcolor="#ffffff">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" align="center">
                                                                <table cellspacing="0" border="0" cellpadding="0" width="750">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top" colspan="2" style="text-align: left;margin: 0px;padding-top:10px;padding-bottom:10px; padding-left:20px; background-color: #DB6A02;" background="#DB6A02" bgcolor="#DB6A02">
                                                                                <a href="https://www.rochester.co.za"><img src="https://www.rochester.co.za/base-theme/images/Rochester-Furniture.png" width="280" height="79" alt="Rochester Furniture"s logo"></a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" height="33" style="height: 33px;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" width="100%" style="padding: 0 40px">
                                                                                <table cellspacing="0" border="0" cellpadding="0" width="100%">
                                
                                                                                        <td style="vertical-align: middle;">
                                                                                            <h4 style="margin: 0; text-align: center; padding: 25px 0 0; font-size: 21px; font-weight: normal; color: #2f436d;">New Application</h4>
                                                                                            <table cellspacing="0" cellpadding="0" border="0" align="center" width="670" bgcolor="#ffffff">
                                                                                            <p style="padding:45px 0 0; text-align: center; font-size:16px; color:#696969;">Great your application has been received and is under processing. We will get in touch to let you know the outcome.Please email us for any queries</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top" height="33" style="height: 33px;">&nbsp;</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" height="40" bgcolor="#ffffff" style="height: 40px;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="middle" height="58" style="height: 58px; vertical-align: middle; border-top-color: #d6d6d6; border-top-width: 1px; border-top-style: solid;">
                                                                                <p style="font-size: 12px; font-weight: normal; color: #333; text-align: center;text-transform:uppercase;">
                                                                                    &copy; ROCHESTER HOME FURNITURE (PTY) LTD.
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <!--/content-->
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--/container-->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </body>

                            </html>';
							
			//load the html we build for new emails
            $email_subject = 'Rochester Furniture - New Finance Application';
            ee()->email->subject($email_subject);
            ee()->email->message($email_msg);
            ee()->email->Send();
        }

        else if ($fields['consumer_application_status']=='APPROVED') {
		
            //send approve email here
            $email_subject = 'Rochester Furniture - Finance Application Approved';

            $email_msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

                            <head>
                                <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                                <title>Rochester Finance Application</title>
                                <!--general stylesheet-->
                                <style type="text/css">
                                body {
                                    background-color: #f6f3e4;
                                }
                                
                                p {
                                    padding: 0;
                                    margin: 0;
                                }
                                
                                h1,
                                h2,
                                h3,
                                h4,
                                p,
                                li,
                                a,
                                th {
                                    font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
                                }
                                
                                td {
                                    vertical-align: top;
                                }
                                
                                ul,
                                ol {
                                    margin: 0;
                                    padding: 0;
                                }
                                
                                .heading {
                                    border-radius: 3px;
                                    -webkit-border-radius: 3px;
                                    -moz-border-radius: 3px;
                                    -khtml-border-radius: 3px;
                                    -icab-border-radius: 3px;
                                }
                                </style>
                            </head>

                            <body marginheight="0" topmargin="0" marginwidth="0" leftmargin="0" background="#f6f3e4" style="margin: 0px; background-color: #f6f3e4;" bgcolor="#f6f3e4">
                                <table cellspacing="0" border="0" cellpadding="0" width="100%" style="margin:40px 0">
                                    <tbody>
                                        <tr valign="top">
                                            <td>
                                                <!--container-->
                                                <table cellspacing="0" cellpadding="0" border="0" align="center" width="750" bgcolor="#ffffff">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" align="center">
                                                                <table cellspacing="0" border="0" cellpadding="0" width="750">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top" colspan="2" style="text-align: left;margin: 0px;padding-top:10px;padding-bottom:10px; padding-left:20px; background-color: #DB6A02;" background="#DB6A02" bgcolor="#DB6A02">
                                                                                <a href="https://www.rochester.co.za"><img src="https://www.rochester.co.za/base-theme/images/Rochester-Furniture.png" width="280" height="79" alt="Rochester Furniture"s logo"></a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" height="33" style="height: 33px;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" width="100%" style="padding: 0 40px">
                                                                                <table cellspacing="0" border="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td valign="middle" height="37" style="height: 37px; vertical-align: middle;">
                                                                                            <h2 style="margin: 0; text-align: center; padding: 0; font-size: 26px; font-weight: normal; color: #2f436d;">Congratulations you have pre-qualified for <br> <b>R100 000 credit*</b></h2>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="vertical-align: middle;padding-top:20px;">
                                                                                            <p style="margin: 0; text-align: center; padding: 10px 0 0; font-size: 16px; font-weight: normal; color: #696969;line-height:26px;">Please complete the application form online via this link:
                                                                                                <br /><a href="https://www.rochester.co.za/finance/application/'.$entry_id.'/" style= "background-color: #337ab7;
																									border-radius: 3px;
																									text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
																									cursor: pointer;
																									padding: 3px 30px;
																									font-size: 12px;
																									line-height: 2.5;
																									color: #eee;
																									text-decoration: none;">View</a></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="vertical-align: middle;padding-top:20px;">
                                                                                            <p style="margin: 0; text-align: center; padding: 10px 0 0; font-size: 16px; font-weight: normal; color: #696969;line-height:26px;">or you can complete the application manually and send it back later:
                                                                                                <br /><a href="https://www.rochester.co.za/finance/download-application-form/'.$entry_id.'/" download="application-form.pdf" style= "background-color: #337ab7;
																									border-radius: 3px;
																									text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
																									cursor: pointer;
																									padding: 3px 30px;
																									font-size: 12px;
																									line-height: 2.5;
																									color: #eee;
																									text-decoration: none;">
                                                                                                Download</a></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <h4 style="margin: 0; text-align: center; padding: 25px 0 0; font-size: 21px; font-weight: normal; color: #2f436d;">Additional Documentation</h4>
                                                                                            <table cellspacing="0" cellpadding="0" border="0" align="center" width="670" bgcolor="#ffffff">
                                                                                                <tr>
                                                                                                    <th colspan="2" style="text-align: center; color: #696969; padding:18px 0 3px; font-weight:normal;">Remember that in order to complete the application you will need the following supporting documents:</th>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <p style="font-size:14px; text-align: center; color:#696969; padding:20px 0 0"><b>Certified copy of your ID (Colour copy not required)</b></p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <p style="font-size:14px; text-align: center; color:#696969; padding:2px 0 0"><b>3 months bank statements</b></p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <p style="font-size:14px; text-align: center; color:#696969; padding:2px 0 0"><b>Copy of your latest payslip</b></p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <p style="font-size:14px; text-align: center; color:#696969; padding:2px 0 0"><b>Optional: Proof of insurance - (life &amp; household insurance)</b></p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <p style="padding:45px 0 0; text-align: center; font-size:16px; color:#696969;">We look forward to receiving your documentation and finalising your application!</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top" height="33" style="height: 33px;">&nbsp;</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" height="40" bgcolor="#ffffff" style="height: 40px;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="middle" height="58" style="height: 58px; vertical-align: middle; border-top-color: #d6d6d6; border-top-width: 1px; border-top-style: solid;">
                                                                                <p style="font-size: 12px; font-weight: normal; color: #333; text-align: center;text-transform:uppercase;">
                                                                                    &copy; ROCHESTER HOME FURNITURE (PTY) LTD.
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <!--/content-->
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--/container-->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </body>

						</html>';
							
			//load the html we build for new emails
            ee()->email->subject($email_subject);
            ee()->email->message($email_msg);
            ee()->email->Send();

        }

        else if ($fields['consumer_application_status']=='DECLINED') {
            //send decline email here
            $email_msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

                            <head>
                                <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                                <title>Rochester Finance Application</title>
                                <!--general stylesheet-->
                                <style type="text/css">
                                body {
                                    background-color: #f6f3e4;
                                }
                                
                                p {
                                    padding: 0;
                                    margin: 0;
                                }
                                
                                h1,
                                h2,
                                h3,
                                h4,
                                p,
                                li,
                                a,
                                th {
                                    font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
                                }
                                
                                td {
                                    vertical-align: top;
                                }
                                
                                ul,
                                ol {
                                    margin: 0;
                                    padding: 0;
                                }
                                
                                .heading {
                                    border-radius: 3px;
                                    -webkit-border-radius: 3px;
                                    -moz-border-radius: 3px;
                                    -khtml-border-radius: 3px;
                                    -icab-border-radius: 3px;
                                }
                                </style>
                            </head>

                            <body marginheight="0" topmargin="0" marginwidth="0" leftmargin="0" background="#f6f3e4" style="margin: 0px; background-color: #f6f3e4;" bgcolor="#f6f3e4">
                                <table cellspacing="0" border="0" cellpadding="0" width="100%" style="margin:40px 0">
                                    <tbody>
                                        <tr valign="top">
                                            <td>
                                                <!--container-->
                                                <table cellspacing="0" cellpadding="0" border="0" align="center" width="750" bgcolor="#ffffff">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" align="center">
                                                                <table cellspacing="0" border="0" cellpadding="0" width="750">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top" colspan="2" style="text-align: left;margin: 0px;padding-top:10px;padding-bottom:10px; padding-left:20px; background-color: #DB6A02;" background="#DB6A02" bgcolor="#DB6A02">
                                                                                <a href="https://www.rochester.co.za"><img src="https://www.rochester.co.za/base-theme/images/Rochester-Furniture.png" width="280" height="79" alt="Rochester Furniture"s logo"></a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" height="33" style="height: 33px;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" width="100%" style="padding: 0 40px">
                                                                                <table cellspacing="0" border="0" cellpadding="0" width="100%">
                                
                                                                                        <td style="vertical-align: middle;">
                                                                                            <h4 style="margin: 0; text-align: center; padding: 25px 0 0; font-size: 21px; font-weight: normal; color: #2f436d;">Application Not Accepted</h4>
                                                                                            <table cellspacing="0" cellpadding="0" border="0" align="center" width="670" bgcolor="#ffffff">
                                                                                            <p style="padding:45px 0 0; text-align: center; font-size:16px; color:#696969;">We regret to inoform you that your applicaion could not be accepted at this moment. Please email us for any queries</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top" height="33" style="height: 33px;">&nbsp;</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" height="40" bgcolor="#ffffff" style="height: 40px;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="middle" height="58" style="height: 58px; vertical-align: middle; border-top-color: #d6d6d6; border-top-width: 1px; border-top-style: solid;">
                                                                                <p style="font-size: 12px; font-weight: normal; color: #333; text-align: center;text-transform:uppercase;">
                                                                                    &copy; ROCHESTER HOME FURNITURE (PTY) LTD.
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <!--/content-->
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--/container-->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </body>

                            </html>'; //load the html we build for decline emails
							
            $email_subject = 'Rochester Furniture - Finance Application Declined';
            ee()->email->subject($email_subject);
            ee()->email->message($email_msg);
            ee()->email->Send();
        }
    }
}
// END CLASS