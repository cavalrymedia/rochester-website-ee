<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array(
	'pi_name'			=> 'URL Ready Function',
	'pi_version'		=> '1.0',
	'pi_author'			=> 'Chris Kempen',
	'pi_author_url'		=> 'http://www.hellocavalry.com/',
	'pi_description'	=> 'Makes sure a URL "piece" is ready for a URL',
	//'pi_usage'			=> '{exp:url_ready piece="Some String To Go In Here"}'
);

class Url_ready {

	public $return_data = '';

	public function Url_ready()
	{
		$this->EE =& get_instance();
		$string = $this->EE->TMPL->fetch_param('piece');

		$this->return_data = str_replace(' ', '-', strtolower($string));
	}
}
?>
