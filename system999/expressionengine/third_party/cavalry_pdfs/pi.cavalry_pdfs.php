<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(dirname(__FILE__) . '/./fpdf/fpdf.php');

class Cavalry_pdfs
{
	public function __construct()
 {
 }
	
	private function drawTableCell($pdf, $col_count, $text = '', $is_heading = false, $goto_next_line = true, $align = 'L', $multiline = false)
	{
		$pdf->SetFont('Arial', '', 7);
		$available_width = 190;
		
		$width = $available_width * ($col_count / 20);
		$height = ($text == '' ? 3 : 4);
		
		if ($multiline)
			$pdf->MultiCell($width, $height, $text, 1, $align, $is_heading);
		else
			$pdf->Cell($width, $height, $text, 1, ($goto_next_line ? 1 : 0), $align, $is_heading);
	}
	
	private function termsColumn($pdf, $col_count, $indent = 0, $rewind_line = false, $text)
	{
		$space = 4.9;
		
		$width = 90;
		$width -= ($indent * $space);
		
		$left = 10;
		$left += ($indent * $space);
		
		if ($col_count != 1)
			$left += 100;
		
		$original_y = $pdf->GetY();
		
		$pdf->SetX($left);
		$pdf->SetFont('Arial', '', 5.5);
		$pdf->MultiCell($width, 2.5, $text);
		
		if ($rewind_line)
			$pdf->SetY($original_y);
	}
	
	public function generate_finance_application_pdf()
	{
		// currently we're only using the first application entry ID...
		$first_application_entry_id = intval(ee()->TMPL->fetch_param('first_application_entry_id'));
		$application_entry_id = intval(ee()->TMPL->fetch_param('application_entry_id'));
		
		// fetch submissions that haven't been processed yet...
		$results = ee()->db->select()->from('freeform_form_entries_30') // pre-application freeform...
			->where(array('entry_id' => $first_application_entry_id))
			->get();
		
		if ($results->num_rows() == 0)
			return false;
		
		// let's build the data...
		$pre_application = $results->result_array()[0];
		
		$title = '';
		$first_name = '';
		$surname = '';
		$initials = '';
		$rsa_id = '';
		$dob = '';
		$marital_status = '';
		$permit_number = '';
		$permit_expiry = '';
		$dwelling_type = '';
		$dwelling_ownership = '';
		$delivery_address = '';
		$employment_type = '';
		$gross_salary = '';
		
		$title_name_surname = explode(' ', $pre_application['form_field_18']);
		
		switch (count($title_name_surname))
		{				
			case 2:
				$first_name = trim($title_name_surname[0]);
				$surname = trim($title_name_surname[1]);
				break;
				
			case 1:
				$first_name = trim($title_name_surname[0]);
				break;
				
			default:
				$title = trim($title_name_surname[0]);
				$surname = trim($title_name_surname[count($title_name_surname)-1]);
				
				unset($title_name_surname[count($title_name_surname)-1]);
				unset($title_name_surname[0]);
				
				$first_name = implode(' ', $title_name_surname);
				
				break;
		}
		
		$first_name_pieces = explode(' ', $first_name);
		foreach ($first_name_pieces as $piece)
			$initials .= $piece[0] . ' ';
		$initials = strtoupper(trim($initials));
		
		$rsa_id = $pre_application['form_field_16'];
		
		if (strlen($rsa_id) > 5)
		{
			$year = intval('22' . substr($rsa_id, 0, 2));
			while ($year > date('Y'))
				$year -= 100;
			$month = intval(substr($rsa_id, 2, 2));
			$day = intval(substr($rsa_id, 4, 2));
			
			$dob_time = mktime(0, 0, 0, $month, $day, $year);
			$dob = date('Y/m/d', $dob_time);
		}
		
		$marital_status = explode('|~|', $pre_application['form_field_53']);
		$marital_status = $marital_status[0];
		
		$permit_number = $pre_application['form_field_159'];
		
		$permit_expiry = $pre_application['form_field_160'];
		
		$dwelling_type = explode('|~|', $pre_application['form_field_128']);
		$dwelling_type = $dwelling_type[0];
		
		$dwelling_ownership = explode('|~|', $pre_application['form_field_127']);
		$dwelling_ownership = $dwelling_ownership[0];
		
		$delivery_address = $pre_application['form_field_80'];
		
		$employment_type = $pre_application['form_field_158'];
		
		$gross_salary = $pre_application['form_field_42'];
		
		$pdf = new FPDF();
		$pdf->SetMargins(10, 10, 10, 10);
		$pdf->SetFillColor(235, 235, 235);
		
		
		
		
		//
		//
		// cover page...
		
		$pdf->AddPage();
		
		$pdf->Image(dirname(__FILE__) . '/./get-it-on-finance.png', 65, 20, 80, 70, 'PNG');
		
		$pdf->SetY(110);
		$pdf->SetFont('Arial', '', 14);
		$pdf->Cell(0, 0, 'Please complete and e-mail this form to creditapplication@rochester.co.za', 0, 1, 'C');
		
		$pdf->SetLineWidth(1.5);
		$pdf->Line(60, 125, 150, 125);
		
		$pdf->SetY(140);
		$pdf->SetFont('Arial', '', 12);
		$pdf->Cell(0, 0, 'In order to process your application, please attach the following documentation:', 0, 1, 'C');
		
		$pdf->SetY(150);
		$pdf->SetFont('Arial', '', 12);
		$pdf->Cell(0, 6, 'Copy of your ID', 0, 1, 'C');
		$pdf->Cell(0, 6, '3 months bank statements', 0, 1, 'C');
		$pdf->Cell(0, 6, 'Proof of your latest payslip', 0, 1, 'C');
		$pdf->Cell(0, 6, 'Proof of insurance (only if applying with your own insurance option)', 0, 1, 'C');
		
		$pdf->SetLineWidth(1.5);
		$pdf->Line(60, 190, 150, 190);
		
		$pdf->SetY(210);
		$pdf->SetFont('Arial', 'I', 12);
		$pdf->Cell(0, 0, 'We look forward to receiving your documentation and finalising your application!', 0, 1, 'C');
		
		
		// end: cover page...
		//
		//
		
		
		
		
		//
		//
		// page 1...
		
		$pdf->AddPage();
		
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(0, 0, 'MANUAL APPLICATION FORM', 0, 1, 'C');
		
		$pdf->SetFont('Arial', '', 6);
		$pdf->Cell(0, 7, 'PRIVATE AND CONFIDENTIAL', 0, 1, 'C');
		
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 9, 'Requested Product Type: Secured (instalment) Loan Unsecured Loan', 0, 1, 'C');
		
		$pdf->SetLineWidth(0.2);
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->Rect(91, 20.25, 2, 2);
		$pdf->Rect(129, 20.25, 2, 2);
		
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(64, 1, 'Secured', 0, 0, 'R');
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(0, 1, 'Credit underwritten by JDG Trading (Pty) Ltd (NCRCP41) and', 0, 1, 'L');
		
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(64, 5, 'Unsecured', 0, 0, 'R');
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(0, 5, 'Credit underwritten by JD Consumer Finance (Pty) Ltd (NCRCP74)', 0, 1, 'L');
		
		//
		//
		// section A...
		
		$pdf->SetY(34);
		
		$this->drawTableCell($pdf, 20, 'SECTION A - PURPOSE OF APPLICATION', true);
		
		$this->drawTableCell($pdf, 3, 'Requested Loan Value:', false, false);
		$this->drawTableCell($pdf, 4, 'R', false, false);
		$this->drawTableCell($pdf, 13, 'Requested Terms Required:', false);
		
		$this->drawTableCell($pdf, 3, 'Life Insurance:', false, false);
		$this->drawTableCell($pdf, 17, 'Basic     or Basic Plus     Comprehensive     or Comprehensive Plus', false);
		$pdf->Rect(46.2, 43, 2, 2);
		$pdf->Rect(64, 43, 2, 2);
		$pdf->Rect(84.7, 43, 2, 2);
		$pdf->Rect(114, 43, 2, 2);
		
		$this->drawTableCell($pdf, 3, 'Goods Insurance:', false, false);
		$this->drawTableCell($pdf, 17, 'Basic Goods and Cellular Phone     Comprehensive Goods and Cellular Phone', false);
		$pdf->Rect(75.8, 47, 2, 2);
		$pdf->Rect(126, 47, 2, 2);
		
		$this->drawTableCell($pdf, 3, 'Own Insurance:', false, false);
		$this->drawTableCell($pdf, 17, 'Yes     No', false);
		$pdf->Rect(44.5, 51, 2, 2);
		$pdf->Rect(51, 51, 2, 2);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		$this->drawTableCell($pdf, 20, 'SECTION A1 - CONSENT BY THE CREDIT APPLICANT', true);
		
		$this->drawTableCell($pdf, 17, 'I consent to the Credit Provider checking my credit record with any credit reference agency.', false, false);
		$this->drawTableCell($pdf, 3, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(183.5, 62, 2, 2);
		$pdf->Rect(190, 62, 2, 2);
		
		$this->drawTableCell($pdf, 17, 'I also consent to the Credit Provider providing credit reference agencies with regular updates about the conduct of my accounts, including the failure to meet the agreed terms and conditions.', false, false, 'L', true);
		$pdf->SetY($pdf->GetY() - 8);
		$pdf->SetX(171.5);
		$this->drawTableCell($pdf, 3, 'Yes     No     ' . "\r\n" . '  ', false, true, 'C', true);
		$pdf->Rect(183.5, 66, 2, 2);
		$pdf->Rect(190, 66, 2, 2);
		
		$this->drawTableCell($pdf, 17, 'I also agree that the credit reference agencies may, in turn, make my record and details available to other Credit Providers.', false, false);
		$this->drawTableCell($pdf, 3, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(183.5, 74, 2, 2);
		$pdf->Rect(190, 74, 2, 2);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		$this->drawTableCell($pdf, 20, 'SECTION A2 - NATIONAL CREDIT ACT (please answer the following questions)', true);
		
		$this->drawTableCell($pdf, 17, '- Are you subject to an administration order?', false, false);
		$this->drawTableCell($pdf, 3, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(183.5, 85, 2, 2);
		$pdf->Rect(190, 85, 2, 2);
		
		$this->drawTableCell($pdf, 17, '- Have you applied to be placed under administration?', false, false);
		$this->drawTableCell($pdf, 3, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(183.5, 89, 2, 2);
		$pdf->Rect(190, 89, 2, 2);
		
		$this->drawTableCell($pdf, 17, '- Are you currently sequestrated?', false, false);
		$this->drawTableCell($pdf, 3, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(183.5, 93, 2, 2);
		$pdf->Rect(190, 93, 2, 2);
		
		$this->drawTableCell($pdf, 17, '- Have you applied to be placed sequestrated?', false, false);
		$this->drawTableCell($pdf, 3, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(183.5, 97, 2, 2);
		$pdf->Rect(190, 97, 2, 2);
		
		$this->drawTableCell($pdf, 17, '- Are you under debt review?', false, false);
		$this->drawTableCell($pdf, 3, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(183.5, 101, 2, 2);
		$pdf->Rect(190, 101, 2, 2);
		
		$this->drawTableCell($pdf, 17, '- Have you applied to be placed under debt review?', false, false);
		$this->drawTableCell($pdf, 3, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(183.5, 105, 2, 2);
		$pdf->Rect(190, 105, 2, 2);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		// end: section A...
		//
		//
		
		//
		//
		// section B...
		
		$this->drawTableCell($pdf, 20, 'SECTION B - PERSONAL DETAILS OF APPLICANT & SPOUSE/PARTNER', true);
		$this->drawTableCell($pdf, 10, 'APPLICANT', true, false);
		$this->drawTableCell($pdf, 10, 'SPOUSE/PARTNER', true);
		
		$this->drawTableCell($pdf, 5, 'Title:', false, false);
		$this->drawTableCell($pdf, 5, $title . ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Title:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Surname:', false, false);
		$this->drawTableCell($pdf, 5, $surname . ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Surname:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'First Name:', false, false);
		$this->drawTableCell($pdf, 5, $first_name . ' ', false, false);
		$this->drawTableCell($pdf, 5, 'First Name:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Initials:', false, false);
		$this->drawTableCell($pdf, 5, $initials . ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Initials:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Identity No (RSA Citizen Only):', false, false);
		$this->drawTableCell($pdf, 5, $rsa_id . ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Identity No (RSA Citizen Only):', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Passport No (RSA Citizen Only):', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Passport No (RSA Citizen Only):', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Date of Birth (YYY/MM/DD):', false, false);
		$this->drawTableCell($pdf, 5, $dob . ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Home Language:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Gender:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Ethnic Group:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Marital Status:', false, false);
		$this->drawTableCell($pdf, 5, $marital_status . ' ', false, false);
		$this->drawTableCell($pdf, 5, 'How are you married:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Number of Dependents:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'If Married, consent received:', false, false);
		$this->drawTableCell($pdf, 5, 'Yes     No', false);
		$pdf->Rect(158.5, 156, 2, 2);
		$pdf->Rect(165, 156, 2, 2);
		
		$this->drawTableCell($pdf, 5, 'South African Citizen:', false, false);
		$this->drawTableCell($pdf, 5, 'Yes     No', false, false);
		$this->drawTableCell($pdf, 10, "\r\n\r\n\r\n\r\n\r\n", true, true, 'L', true);
		$pdf->Rect(63.5, 160, 2, 2);
		$pdf->Rect(70, 160, 2, 2);
		
		$pdf->SetY(163);
		$this->drawTableCell($pdf, 5, 'If No, which country:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Temporary Resident Permit No:', false, false);
		$this->drawTableCell($pdf, 5, $permit_number . ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Permit Expiry Date (YYYY/MM/DD):', false, false);
		$this->drawTableCell($pdf, 5, $permit_expiry . ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Employee Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		$this->drawTableCell($pdf, 20, 'SECTION B2 - CONTACT DETAILS OF APPLICANT & SPOUSE/PARTNER', true);
		$this->drawTableCell($pdf, 5, 'CONTACT DETAILS', true, false);
		$this->drawTableCell($pdf, 5, 'APPLICANT', true, false);
		$this->drawTableCell($pdf, 10, 'SPOUSE/PARTNER', true);
		
		$this->drawTableCell($pdf, 5, 'Home Telephone Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Work Telephone Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Work Telephone Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Cellular Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Cellular Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'E-Mail Address:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Alternative Cellular Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Alternative E-Mail Address:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'E-Mail Address:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Fax Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Alternative E-Mail Address:', false, false);
		$this->drawTableCell($pdf, 15, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Fax Number:', false, false);
		$this->drawTableCell($pdf, 15, ' ', false);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		// end: section B...
		//
		//
		
		//
		//
		// section C...
		
		$this->drawTableCell($pdf, 20, 'SECTION C - DETAILS OF PROPERTY/RESIDENCE', true);
		$this->drawTableCell($pdf, 10, 'PRESENT RESIDENTIAL DETAILS', true, false);
		$this->drawTableCell($pdf, 10, 'PREVIOUS RESIDENTIAL DETAILS', true);
		
		$this->drawTableCell($pdf, 5, 'Name of Building/Complex & No:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Name of Building/Complex & No:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Period of Residence:', false, false);
		$this->drawTableCell($pdf, 1.25, 'MM', false, false, 'C');
		$this->drawTableCell($pdf, 1.25, ' ', false, false, 'C');
		$this->drawTableCell($pdf, 1.25, 'YY', false, false, 'C');
		$this->drawTableCell($pdf, 1.25, ' ', false, false, 'C');
		$this->drawTableCell($pdf, 5, 'Period of Residence:', false, false);
		$this->drawTableCell($pdf, 1.25, 'MM', false, false, 'C');
		$this->drawTableCell($pdf, 1.25, ' ', false, false, 'C');
		$this->drawTableCell($pdf, 1.25, 'YY', false, false, 'C');
		$this->drawTableCell($pdf, 1.25, ' ', false, true, 'C');
		
		$this->drawTableCell($pdf, 5, 'Name of Street & Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Name of Street & Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Suburb:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Suburb:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Town:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Town:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Province:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Province:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Postal Code:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Postal Code:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$pdf->SetY(275);
		$pdf->Cell(0, 0, 'Doc Ref: Third Party Application for Credit Form (FS-068-F) | Version 2014, Issue 01 | Effective Date: 2014/11/17', 0, 0);
		$pdf->Cell(0, 0, 'Page 1 of 3', 0, 1, 'R');
		$pdf->Line(10, 272, 200, 272);
		
		// end: section C...
		//
		//
		
		// end: page 1...
		//
		//
		
		//
		//
		// page 2...
		
		$pdf->AddPage();
		
		//
		//
		// section C continued...
		
		$pdf->setY(14);
		$this->drawTableCell($pdf, 20, 'SECTION C - DETAILS OF PROPERTY/RESIDENCE (continued)', true);
		
		$this->drawTableCell($pdf, 6, 'Type of Dwelling:', false, false);
		$this->drawTableCell($pdf, 7, $dwelling_type . ' ', false, false);
		$this->drawTableCell($pdf, 7, 'PRESENT POSTAL ADDRESS', true);
		
		$this->drawTableCell($pdf, 6, 'Ownership:', false, false);
		$this->drawTableCell($pdf, 7, $dwelling_ownership . ' ', false, false);
		$this->drawTableCell($pdf, 7, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Does Applicant Own a Motor Vehicle:', false, false);
		$this->drawTableCell($pdf, 7, 'Yes     No', false, false);
		$this->drawTableCell($pdf, 7, ' ', false);
		$pdf->Rect(73, 27, 2, 2);
		$pdf->Rect(79.5, 27, 2, 2);
		
		$this->drawTableCell($pdf, 20, 'DELIVERY ADDRESS DETAILS', true);
		$this->drawTableCell($pdf, 20, $delivery_address . ' ', false);
		$this->drawTableCell($pdf, 20, ' ', false);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		// end: section C...
		//
		//
		
		//
		//
		// section D...
		
		$this->drawTableCell($pdf, 20, 'SECTION D - EMPLOYMENT DETAILS', true);
		$this->drawTableCell($pdf, 10, 'CURRENT EMPLOYER', true, false);
		$this->drawTableCell($pdf, 10, 'PREVIOUS EMPLOYER (IF LESS THAN 5 YEARS)', true);
		
		$this->drawTableCell($pdf, 6, 'Employment Type:', false, false);
		$this->drawTableCell($pdf, 4, $employment_type . ' ', false, false);
		$this->drawTableCell($pdf, 6, 'Company / Employer Name:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Employment Sector:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false, false);
		$this->drawTableCell($pdf, 6, 'Address Line 1:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Company / Employer Name:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false, false);
		$this->drawTableCell($pdf, 6, 'Address Line 2:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Address Line 1:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false, false);
		$this->drawTableCell($pdf, 6, 'Suburb:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Address Line 2:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false, false);
		$this->drawTableCell($pdf, 6, 'Town:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Suburb:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false, false);
		$this->drawTableCell($pdf, 6, 'Province:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Town:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false, false);
		$this->drawTableCell($pdf, 6, 'Postal Code:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Province:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false, false);
		$this->drawTableCell($pdf, 6, 'Period Employed (MM/YYYY):', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Postal Code:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false, false);
		$this->drawTableCell($pdf, 6, 'Employer Telephone Number:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Period Employed (MM/YYYY):', false, false);
		$this->drawTableCell($pdf, 4, ' ', false, false);
		$this->drawTableCell($pdf, 10, "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n", true, true, 'L', true);
		
		$pdf->SetY($pdf->GetY() - 36);
		$this->drawTableCell($pdf, 6, 'Employer Telephone Number:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Employee No/Clock No:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Nickname:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Supervisor Name:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Department:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'HR Contact Person:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Salary Pay Day:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'Salary Frequency:', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 6, 'If Contractor, when does contract end (MM/YYYY):', false, false);
		$this->drawTableCell($pdf, 4, ' ', false);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		// end: section D...
		//
		//
		
		//
		//
		// section E...
		
		$this->drawTableCell($pdf, 20, 'SECTION E - DETAILS OF 2 REFERENCES NOT LIVING WITH YOU', true);
		
		$this->drawTableCell($pdf, 5, 'REFERENCE DETAILS', true, false);
		$this->drawTableCell($pdf, 5, 'REFERENCE 1', true, false);
		$this->drawTableCell($pdf, 5, ' ', true, false);
		$this->drawTableCell($pdf, 5, 'REFERENCE 2', true);
		
		$this->drawTableCell($pdf, 5, 'Surname:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Surname:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'First Name:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'First Name:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Relationship:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Relationship:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Residential Address Line 1:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Residential Address Line 1:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Residential Address Line 2:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Residential Address Line 2:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Town:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Town:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Province:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Province:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Postal Code:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Postal Code:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Home Telephone Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Home Telephone Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Work Telephone Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Work Telephone Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Cellular Telephone Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Cellular Telephone Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		// end: section E...
		//
		//
		
		//
		//
		// section F...
		
		$this->drawTableCell($pdf, 20, 'SECTION F - BANK ACCOUNT DETAILS / DEBIT ORDER AUTHORISATION', true);
		
		$this->drawTableCell($pdf, 5, 'Account Holder\'s Surname & Initials:', false, false);
		$this->drawTableCell($pdf, 15, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Bank Account Type:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Credit Card Type:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Bank Name:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Card Number:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Bank Branch Name:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Expiry Date (MM/YYYY):', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Branch Code:', false, false);
		$this->drawTableCell($pdf, 15, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Account Number:', false, false);
		$this->drawTableCell($pdf, 15, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Method of Payment:', false, false);
		$this->drawTableCell($pdf, 5, 'Debit Order', false, false);
		$this->drawTableCell($pdf, 5, 'Debit Order Date:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$message = 'I, the undersigned, declare that I am the account holder of the above mentioned bank account. I hereby authorise the Credit Provider '.
			'to debit my account as detailed above, with the monthly instalment amount payable, alternatively the full amount due in terms of the credit agreement, on or after my '.
			'pay day until such a time when the account is settled. I authorise the Credit Provider to deduct under certain circumtances on a date different to my debit order '.
			'date. I agree that the debit order may be cancelled should the company not be able to collect funds from my account for 3 consecutive months.';
		$this->drawTableCell($pdf, 20, $message, false, true, 'L', true);
		
		$this->drawTableCell($pdf, 10, ' ', false, false , 'C');
		$this->drawTableCell($pdf, 10, ' ', false, true , 'C');
		$this->drawTableCell($pdf, 10, 'SIGNATURE OF ACCOUNT HOLDER', false, false , 'C');
		$this->drawTableCell($pdf, 10, 'DATE (YYYY/MM/DD)', false, true , 'C');
		
		$pdf->SetY(275);
		$pdf->Cell(0, 0, 'Doc Ref: Third Party Application for Credit Form (FS-068-F) | Version 2014, Issue 01 | Effective Date: 2014/11/17', 0, 0);
		$pdf->Cell(0, 0, 'Page 2 of 3', 0, 1, 'R');
		$pdf->Line(10, 272, 200, 272);
		
		// end: section F...
		//
		//
		
		// end: page 2...
		//
		//
		
		//
		//
		// page 3...
		
		$pdf->AddPage();
		
		//
		//
		// section G...
		
		$pdf->setY(14);
		
		$this->drawTableCell($pdf, 20, 'SECTION G - MONTHLY AFFORDABILITY CALCULATION (Rands)', true);
		
		$this->drawTableCell($pdf, 5, 'INCOME - APPLICANT', true, false);
		$this->drawTableCell($pdf, 5, 'AMOUNT', true, false, 'C');
		$this->drawTableCell($pdf, 5, 'INCOME - SPOUSE/PARTNER', true, false);
		$this->drawTableCell($pdf, 5, 'AMOUNT', true, true, 'C');
		
		$this->drawTableCell($pdf, 5, 'Gross Salary:', false, false);
		$this->drawTableCell($pdf, 5, $gross_salary . ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Gross Salary:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Gross Overtime:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Gross Overtime:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Nett Salary:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Nett Salary:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Child Grant/Support:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 10, "\r\n\r\n", true, true, 'L', true);
		
		$pdf->SetY($pdf->GetY() - 4);
		$this->drawTableCell($pdf, 5, 'Government Grant:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Rental Income:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Rental Income:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Other Income:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Other Income:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		$this->drawTableCell($pdf, 5, 'EXPENDITURE - APPLICANT', true, false);
		$this->drawTableCell($pdf, 5, 'AMOUNT', true, false, 'C');
		$this->drawTableCell($pdf, 5, 'EXPENDITURE - SPOUSE/PARTNER', true, false);
		$this->drawTableCell($pdf, 5, 'AMOUNT', true, true, 'C');
		
		$this->drawTableCell($pdf, 5, 'Home Loan Account:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Home Loan Account:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Home Rental:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Home Rental:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Furniture Retail Account/s:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Furniture Retail Account/s:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Clothing Account/s:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Clothing Account/s:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Credit Cards:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Credit Cards:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Personal Loans:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Personal Loans:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Micro Loans:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Micro Loans:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Educational Loans:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Educational Loans:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Business Loans:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Business Loans:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Car Finance:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Car Finance:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Insurance (Short & Long Term):', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Insurance (Short & Long Term):', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Fixed Line Contract:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Fixed Line Contract:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Cellular Contract:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Cellular Contract:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Internet Contract:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Internet Contract:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		$this->drawTableCell($pdf, 10, 'LIVING EXPENSES (NOT PAID ON CREDIT)', true, false);
		$this->drawTableCell($pdf, 10, 'LIVING EXPENSES (NOT PAID ON CREDIT)', true);
		
		$this->drawTableCell($pdf, 5, 'APPLICANT', true, false);
		$this->drawTableCell($pdf, 5, 'AMOUNT', true, false, 'C');
		$this->drawTableCell($pdf, 5, 'SPOUSE/PARTNER', true, false);
		$this->drawTableCell($pdf, 5, 'AMOUNT', true, true, 'C');
		
		$this->drawTableCell($pdf, 5, 'Groceries:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Groceries:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'School Fees:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'School Fees:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Transport Costs:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Transport Costs:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Pre-Paid Fixed Line:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Pre-Paid Fixed Line:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Pre-Paid Cellular:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Pre-Paid Cellular:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Pre-Paid Internet Line:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Pre-Paid Internet Line:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Other Credit Contracts:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Other Credit Contracts:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Utilities (Water & Lights, Rates):', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Utilities (Water & Lights, Rates):', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 5, 'Other Expenses:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false, false);
		$this->drawTableCell($pdf, 5, 'Other Expenses:', false, false);
		$this->drawTableCell($pdf, 5, ' ', false);
		
		$this->drawTableCell($pdf, 20, '', false);
		
		// end: section G...
		//
		//
		
		//
		//
		// section H...
		
		$this->drawTableCell($pdf, 20, 'SECTION H - ACKNOWLEDGEMENT, CERTIFICATION, and AUTHORISATION BY THE CREDIT APPLICANT', true);
		
		$this->drawTableCell($pdf, 20, "CREDIT INSURANCE\r\nIn terms of section 106 of the NCA a consumer is required to maintain adequate insurance during the term of their credit agreement.", false, true, 'L', true);
		$this->drawTableCell($pdf, 20, '', false);
		
		$this->drawTableCell($pdf, 20, 'MARKETING INFORMATION');
		
		$this->drawTableCell($pdf, 16, 'May we or any of our associated companies market to you?', false, false);
		$this->drawTableCell($pdf, 4, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(178.7, 183, 2, 2);
		$pdf->Rect(185.2, 183, 2, 2);
		
		$this->drawTableCell($pdf, 16, 'Preferred correspondence method for marketing communication?', false, false);
		$this->drawTableCell($pdf, 4, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(178.7, 187, 2, 2);
		$pdf->Rect(185.2, 187, 2, 2);
		
		$this->drawTableCell($pdf, 16, 'How did you hear about us?', false, false);
		$this->drawTableCell($pdf, 4, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(178.7, 191, 2, 2);
		$pdf->Rect(185.2, 191, 2, 2);
		
		$this->drawTableCell($pdf, 16, 'Reason for loan?', false, false);
		$this->drawTableCell($pdf, 4, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(178.7, 195, 2, 2);
		$pdf->Rect(185.2, 195, 2, 2);
		
		$this->drawTableCell($pdf, 16, 'How would you like to receive your statements?', false, false);
		$this->drawTableCell($pdf, 4, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(178.7, 199, 2, 2);
		$pdf->Rect(185.2, 199, 2, 2);
		
		$this->drawTableCell($pdf, 16, 'What is your preferred language for correspondence?', false, false);
		$this->drawTableCell($pdf, 4, 'Yes     No     ', false, true, 'C');
		$pdf->Rect(178.7, 203, 2, 2);
		$pdf->Rect(185.2, 203, 2, 2);
		
		// end: section H...
		//
		//
		
		$pdf->SetDrawColor(180, 180, 180);
		$pdf->SetY($pdf->GetY() + 13);
		$pdf->Cell(50, 5, 'Signature of Consumer', 'T', 0, 'C');

		$pdf->SetX(75);
		$pdf->Cell(60, 5, '(1) Witness to Consumer', 'T', 0, 'C');
		
		$pdf->SetX(150);
		$pdf->Cell(50, 5, 'Date', 'T', 0, 'C');
		
		
		
		$pdf->SetDrawColor(180, 180, 180);
		$pdf->SetY($pdf->GetY() + 14);
		$pdf->Cell(50, 5, 'Signature of Consumer', 'T', 0, 'C');
		
		$pdf->SetX(75);
		$pdf->Cell(60, 5, '(1) Witness to Spouse', 'T', 0, 'C');
		
		$pdf->SetX(150);
		$pdf->Cell(50, 5, 'Date', 'T', 0, 'C');
		
		$pdf->SetY($pdf->GetY() + 3);
		$pdf->SetFont('Arial', '', 5);
		$pdf->Cell(50, 5, '(if married in community of property)', 0, 0, 'C');
		
		
		
		$pdf->SetFont('Arial', '', 7);
		$pdf->SetDrawColor(180, 180, 180);
		$pdf->SetY($pdf->GetY() + 10);
		$pdf->SetX(75);
		$pdf->Cell(60, 5, '(2) Witness to Spouse', 'T', 0, 'C');
		
		
		
		$pdf->SetY(275);
		$pdf->Cell(0, 0, 'Doc Ref: Third Party Application for Credit Form (FS-068-F) | Version 2014, Issue 01 | Effective Date: 2014/11/17', 0, 0);
		$pdf->Cell(0, 0, 'Page 3 of 3', 0, 1, 'R');
		$pdf->Line(10, 272, 200, 272);
		
		// end: page 3...
		//
		//
		
		//
		//
		// terms and conditions page 1...
		
		$pdf->AddPage();
		
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 0, 'SECURED LOAN AGREEMENT', 0, 1, 'C');
		
		// column 1...
		
		$pdf->SetY(16);
		$this->termsColumn($pdf, 1, 0, false, "The credit provider and the consumer hereby conclude an instalment agreement subject to the terms and conditions of this agreement, read with the schedule to the agreement.\r\n\r\n");
		
		$this->termsColumn($pdf, 1, 0, false, "1. SALE AGREEMENT");
		$this->termsColumn($pdf, 1, 1, true, "1.1.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer hereby agrees to purchase from the credit provider the goods and/or services set out in the schedule to this agreement and to pay to the credit provider the amounts set out in the schedule to this agreement, subject to the terms and conditions set out herein");
		
		$this->termsColumn($pdf, 1, 1, true, "1.2.");
		$this->termsColumn($pdf, 1, 2, false, "The parties record that this agreement is an installment agreement as defined in the National Credit Act No.34 of 2005 as amended (NCA) and that the NCA is applicable to this agreement.");
		
		$this->termsColumn($pdf, 1, 1, true, "1.3.");
		$this->termsColumn($pdf, 1, 2, false, "The schedule to this agreement, (“the Schedule”) setting out information regarding the goods and/or services purchased, the credit extended and the payment terms, forms part of this agreement and must be read as such.");
		
		$this->termsColumn($pdf, 1, 1, true, "1.4.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer acknowledges that he/she has received a Quotation and Pre-agreement Statement (which includes a Summary of Rights, Obligations and Security) in terms of the NCA.");
		
		$this->termsColumn($pdf, 1, 0, false, "2. POSSESSION, OWNERSHIP, USE AND RISK OF THE GOODS");
		$this->termsColumn($pdf, 1, 1, true, "2.1.");
		$this->termsColumn($pdf, 1, 2, false, "Possession of the goods and/or services shall be given to the consumer on signature of this agreement and against payment of the deposit (if any), referred to in the schedule.");
		
		$this->termsColumn($pdf, 1, 1, true, "2.2.");
		$this->termsColumn($pdf, 1, 2, false, "Notwithstanding delivery, ownership of the goods shall remain vested in the credit provider and shall not pass to the consumer until all amounts owing under the agreement have been paid in full.");

		$this->termsColumn($pdf, 1, 1, true, "2.3.");
		$this->termsColumn($pdf, 1, 2, false, "Should the goods purchased in terms of this agreement be delivered to the consumer by any supplier other than the credit provider, the consumer shall take delivery of the goods on behalf of the credit provider, who upon delivery shall become the owner of the goods and shall remain the owner until ownership passes to the consumer in terms of clause 2.2.");
		
		$this->termsColumn($pdf, 1, 1, true, "2.4.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer shall at all times during the duration of the agreement keep the goods in his/ her possession and control and shall take reasonable care in the use of the goods and shall at his/her own cost and expense, maintain the goods in proper order and protect them against loss or damage.");
		
		$this->termsColumn($pdf, 1, 1, true, "2.5.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer shall furthermore, at his/her own expense, keep the goods free from attachment, hypothec or other legal charge or process and shall not, without the prior written consent of the credit provider, sell, let, lend, pledge, transfer or otherwise encumber the goods in any way or permit any lien to arise in respect of the goods.");
		$this->termsColumn($pdf, 1, 1, true, "2.6.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer shall immediately, in writing, notify the owner or lessor of any premises into which the goods are brought, of the credit provider's ownership of the goods and the credit provider shall be entitled to give such notice to the owner or lessor on behalf of the consumer.");
		$this->termsColumn($pdf, 1, 1, true, "2.7.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer shall not materially alter or modify the goods without the prior written consent of the credit provider. Any part or accessory added to and/or attached to the goods shall become the credit provider's property without any compensation, until the consumer's obligations, in respect of this agreement, have been settled in full.");
		$this->termsColumn($pdf, 1, 1, true, "2.8.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer acknowledges and agrees that the goods are movable property and shall remain so, notwithstanding the means used to install them and the goods shall not under any circumstances accede to the property where they are installed or kept.");
		$this->termsColumn($pdf, 1, 1, true, "2.9.");
		$this->termsColumn($pdf, 1, 2, false, "The risk regarding loss of or damage to the goods shall pass to the consumer on delivery of the goods.");
		$this->termsColumn($pdf, 1, 1, true, "2.10.");
		$this->termsColumn($pdf, 1, 2, false, "Until the termination of this agreement for whatever reason, the consumer shall be obliged, within 10 business days and on Form 24 prescribed in terms of the Regulations to the NCA, to inform the credit provider in the prescribed time, manner and form of any change concerning:");
		
		$this->termsColumn($pdf, 1, 2, true, "2.10.1.");
		$this->termsColumn($pdf, 1, 4, false, "the consumer's residential or business address and");
		$this->termsColumn($pdf, 1, 2, true, "2.10.2.");
		$this->termsColumn($pdf, 1, 4, false, "the address of the premises, in which the goods are ordinarily kept and;");
		$this->termsColumn($pdf, 1, 2, true, "2.10.3.");
		$this->termsColumn($pdf, 1, 4, false, "the name and address of any other person to whom possession of the goods been given.");
		
		$this->termsColumn($pdf, 1, 1, true, "2.11.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer's attention is drawn to the fact that failure to comply with the provisions of the previous subparagraph constitutes an offence.");
		
		$this->termsColumn($pdf, 1, 0, false, "3. THE PRINCIPAL DEBT");
		$this->termsColumn($pdf, 1, 1, true, "3.1.");
		$this->termsColumn($pdf, 1, 2, false, "The principal debt applicable to this agreement is the credit advanced by the credit provider to the consumer, being the total purchase price of the goods and/or services, plus the additional charges permitted by section 102 of the NCA, as detailed in the schedule.");
		$this->termsColumn($pdf, 1, 1, true, "3.2.");
		$this->termsColumn($pdf, 1, 2, false, "It is recorded that the consumer has the option of paying the initiation fee reflected in the schedule upfront.");
		
		$this->termsColumn($pdf, 1, 0, false, "4. SERVICE FEE");
		$this->termsColumn($pdf, 1, 1, true, "4.1.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer will be charged a pro rata service fee in the first month of billing which is subject to full delivery of the goods.");
		$this->termsColumn($pdf, 1, 1, true, "4.2.");
		$this->termsColumn($pdf, 1, 2, false, "The credit provider shall be entitled to charge the consumer the maximum monthly service fee permitted in terms of the NCA and the regulations thereto.");
		$this->termsColumn($pdf, 1, 1, true, "4.3.");
		$this->termsColumn($pdf, 1, 2, false, "Any reduction of the service fee granted to the consumer (including the charging of a discounted service fee at the commencement of the agreement) shall be entirely within the credit provider's discretion and may at any time be increased to the maximum permissible amount, subject to such notice as may be required by the NCA, if any, being given to the consumer. ");
		
		$this->termsColumn($pdf, 1, 0, false, "5. INSURANCE");
		$this->termsColumn($pdf, 1, 1, true, "5.1.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer shall enter into a short-term insurance agreement with an insurer of his/her own choice, which is registered in terms of the Short Term Insurance Act 1998, in terms of which the goods sold in terms of this agreement will be insured against loss, theft or damage for an insured amount not exceeding the consumer's outstanding obligations in terms of this agreement.");
		$this->termsColumn($pdf, 1, 1, true, "5.2.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer shall enter into a credit life insurance agreement with an insurer of his/her choice, which is registered in terms of the Long Term Insurance Act 1998, in terms of which he/she is insured against death and disability for an insured amount not exceeding the consumer's outstanding obligations in terms of this agreement.");
		$this->termsColumn($pdf, 1, 1, true, "5.3.");
		$this->termsColumn($pdf, 1, 2, false, "The credit provider may offer to the consumer further optional insurance relating to the possession, use, ownership or benefits of the goods sold in terms of this agreement.");
		$this->termsColumn($pdf, 1, 1, true, "5.4.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer hereby authorises the credit provider to pay the insurance premiums in respect of the aforesaid policies, as set out in the schedule to this agreement, on behalf of  the consumer to the insurer(s), and to recover such amount(s) as paid on behalf of the consumer.");
		$this->termsColumn($pdf, 1, 1, true, "5.5.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer shall have the right to reject any particular insurance policy proposed by the credit provider and to substitute a policy of his or her own choice, provided that such policy complies with the terms of this agreement.");
		$this->termsColumn($pdf, 1, 1, true, "5.6.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer hereby admits that he/she has exercised a free choice in respect of the insurer with which the aforementioned insurance policy/ies is concluded. Further, the consumer confirms that he/she had an unqualified unrestricted free choice as to:");
		$this->termsColumn($pdf, 1, 2, true, "5.6.1.");
		$this->termsColumn($pdf, 1, 4, false, "whether a new policy(ies) is taken out or whether an existing policy(ies) is used for the purpose of clause 6.1 and 6.2;");
		$this->termsColumn($pdf, 1, 2, true, "5.6.2.");
		$this->termsColumn($pdf, 1, 4, false, "which short-term or long-term insurer issues the policy(ies) and which institution or person will act as intermediary; and");
		$this->termsColumn($pdf, 1, 2, true, "5.6.3.");
		$this->termsColumn($pdf, 1, 4, false, "that such free choice was exercised freely without any coercion or inducement");
		
		// end: column 1...
		
		// column 2...
		
		$pdf->SetY(16);
		
		$this->termsColumn($pdf, 2, 4, false, "as to the manner in which he/she exercised such free choice.");
		$this->termsColumn($pdf, 2, 1, true, "5.7.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer confirms that he/she understands his/her freedom of choice as explained and that such freedom of choice was explained to him/her before any decision was made as to what policy(ies) to utilise for the purposes of clause 5.1 and 5.2.Should the consumer choose to substitute a policy of his or her own choice the consumer undertakes to give written proof to the satisfaction of the credit provider of the policy so substituted in terms of this agreement before the delivery of the goods in terms of this agreement.");
		$this->termsColumn($pdf, 2, 1, true, "5.8.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer hereby cedes the aforementioned insurance policy/policies (whether  proposed by the credit provider or substituted by the consumer) to the credit provider to secure the consumer's indebtedness in terms of this agreement. 5.9. The consumer shall notify the credit provider immediately of any potential claim in terms of the above-mentioned insurance policy or policies and shall fully comply with all the terms of such insurance policies.");
		
		$this->termsColumn($pdf, 2, 0, false, "6. INTEREST");
		$this->termsColumn($pdf, 2, 1, true, "6.1.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer shall be obliged to pay interest on the balance of the principal debt from time to time at the rate specified in the Schedule, calculated daily and capitalised monthly in arrears on the date the monthly installments are payable in terms of the schedule.");
		$this->termsColumn($pdf, 2, 1, true, "6.2.");
		$this->termsColumn($pdf, 2, 2, false, "In the event that the consumer fails to pay any installment or any other amount due on due date, such overdue amounts shall bear interest at the maximum interest rate applicable to an agreement of this nature, as prescribed the National Credit Act and any Regulations thereto, at the specific time of default.");
		$this->termsColumn($pdf, 2, 1, true, "6.3.");
		$this->termsColumn($pdf, 2, 2, false, "Subject to the provisions of the NCA and the Regulations thereto, the interest rate shall be a fixed rate.");
		$this->termsColumn($pdf, 2, 1, true, "6.4.");
		$this->termsColumn($pdf, 2, 2, false, "The calculation of interest shall be in accordance with the NCA and the regulations thereto.");
		
		$this->termsColumn($pdf, 2, 0, false, "7. PAYMENT OF INSTALLMENTS");
		$this->termsColumn($pdf, 2, 1, true, "7.1.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer shall pay to the credit provider the installments specified in the schedule to this agreement.");
		$this->termsColumn($pdf, 2, 1, true, "7.2.");
		$this->termsColumn($pdf, 2, 2, false, "Any installment due in terms of this agreement is due and payable on or before the last day of each calendar month unless prior alternative written arrangement is made with the credit provider.");
		$this->termsColumn($pdf, 2, 1, true, "7.3.");
		$this->termsColumn($pdf, 2, 2, false, "Should the consumer enter into this agreement and receive full delivery of the goods on or before the 15th day of the month, the first installment will be due and payable on or before the last day of the same month.");
		$this->termsColumn($pdf, 2, 1, true, "7.4.");
		$this->termsColumn($pdf, 2, 2, false, "Should the consumer enter into this agreement and receive full delivery on or after the 15th day of the month, the first installment will be due and payable on or before the last day of the following month.");
		$this->termsColumn($pdf, 2, 1, true, "7.5.");
		$this->termsColumn($pdf, 2, 2, false, "Subject to the consumer's rights in terms of the common law, he/she shall not be entitled to withhold payment of any installments or other amounts owing to the credit provider. The consumer will not be entitled to set off against any installments or other amounts payable in terms hereof, any present or future claim, which the consumer may have against the credit provider, from whatever cause arising.");
		$this->termsColumn($pdf, 2, 1, true, "7.6.");
		$this->termsColumn($pdf, 2, 2, false, "All installments shall be paid by way of a standard or an early debit order (the choice of which will be at the credit provider's election), and the consumer authorises the credit provider to instruct the consumer's bank to deduct a variable amount directly from the consumer's bank account and to pay the amount due to the credit provider. The variable amount is the monthly installment, as well as any other amounts that may be due, from time to time, by the consumer in terms of this agreement.");
		$this->termsColumn($pdf, 2, 1, true, "7.7.");
		$this->termsColumn($pdf, 2, 2, false, "The debit order authorisation and mandate in terms of this agreement may be ceded or assigned to a third party if the credit provider cedes or assigns the consumer's indebtedness to it in terms of this agreement to that party.");
		$this->termsColumn($pdf, 2, 1, true, "7.8.");
		$this->termsColumn($pdf, 2, 2, false, "Subject to clause 22, the credit provider may allow the consumer to make payment at the address of the credit provider, as detailed in the schedule or by such other method that the credit provider may deem fit.");
		$this->termsColumn($pdf, 2, 1, true, "7.9.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer may at any time, without notice or penalty, prepay any amount due to the credit provider under this agreement. The credit provider will credit each payment made under the agreement to the consumer as of the date of receipt by the credit provider of that payment, and will do so as follows:");
		$this->termsColumn($pdf, 2, 2, true, "7.9.1.");
		$this->termsColumn($pdf, 2, 4, false, "firstly to satisfy any due or unpaid interest;");
		$this->termsColumn($pdf, 2, 2, true, "7.9.2.");
		$this->termsColumn($pdf, 2, 4, false, "secondly to satisfy any due or unpaid fees or charges; and");
		$this->termsColumn($pdf, 2, 2, true, "7.9.3.");
		$this->termsColumn($pdf, 2, 4, false, "thirdly to reduce the amount of the principal debt.");
		
		$this->termsColumn($pdf, 2, 0, false, "8. STATEMENTS OF ACCOUNT");
		$this->termsColumn($pdf, 2, 1, true, "8.1.");
		$this->termsColumn($pdf, 2, 2, false, "The credit provider shall deliver to the consumer a statement of account in the form prescribed by the NCA.");
		$this->termsColumn($pdf, 2, 1, true, "8.2.");
		$this->termsColumn($pdf, 2, 2, false, "Such statements shall be delivered at regular intervals, not exceeding three months.");
		$this->termsColumn($pdf, 2, 1, true, "8.3.");
		$this->termsColumn($pdf, 2, 2, false, "The statements shall be delivered to the consumer as per the consumer's preferred method of delivery reflected in the schedule.");
		$this->termsColumn($pdf, 2, 1, true, "8.4.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer shall be entitled to dispute all or part of any incorrect credit or debit in a statement of account by delivering a written notice to the credit provider.");
		
		$this->termsColumn($pdf, 2, 0, false, "9. EARLY SETTLEMENT");
		$this->termsColumn($pdf, 2, 1, true, "9.1.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer is entitled to settle this agreement at any time either with or without notice to the credit provider, by paying the settlement amount.");
		$this->termsColumn($pdf, 2, 1, true, "9.2.");
		$this->termsColumn($pdf, 2, 2, false, "The amount, which is required to settle this agreement (“settlement amount”) is the total of the unpaid balance of the principal debt at the time and the unpaid interest charges and all other fees and charges payable by the consumer to the credit provider up to the settlement date.");
		
		$this->termsColumn($pdf, 2, 0, false, "10. BREACH, DEBT COLLECTION AND COLLECTION CHARGES");
		$this->termsColumn($pdf, 2, 1, true, "10.1.");
		$this->termsColumn($pdf, 2, 2, false, "In the event of the consumer failing to pay any amount due in terms of this agreement, the credit provider shall be entitled to either:");
		$this->termsColumn($pdf, 2, 2, true, "10.1.1.");
		$this->termsColumn($pdf, 2, 4, false, "immediately submit an early debit order instruction to the consumer's bank to collect the outstanding amount, notwithstanding that such instruction is presented to the consumer's bank in the same month as the consumer  failed to pay;");
		$this->termsColumn($pdf, 2, 2, true, "10.1.2.");
		$this->termsColumn($pdf, 2, 4, false, "deduct such arrear amount from the consumer's bank account through an  additional debit order, which deduction made through an additional debit  order will be without prejudice to the credit provider's rights in terms of  this agreement;");
		$this->termsColumn($pdf, 2, 2, true, "10.1.3.");
		$this->termsColumn($pdf, 2, 4, false, "track the consumer's bank account and the amount owed by consumer  the will be deducted and paid to the credit provider as soon as there are  sufficient funds in the account, in which event payment may occur on a date  that is not the consumer's usual debit order deduction date;");
		$this->termsColumn($pdf, 2, 2, true, "10.1.4.");
		$this->termsColumn($pdf, 2, 4, false, "instruct a firm of debt collectors registered in terms of the Debt Collectors  Act, Act 114 of 1998 or a firm of attorneys to collect payment of the amount  due in terms of the agreement on behalf of the credit provider.");
		$this->termsColumn($pdf, 2, 1, true, "10.2.");
		$this->termsColumn($pdf, 2, 2, false, "Subject to the provision of the Debt Collectors Act and the Regulations thereto, any debt collector collecting the debt due to the credit provider shall be entitled to make contact with and demand payment from the consumer by way of personal or telephonic consultations, send or deliver letters of demand to the consumer, or to take any other lawful step to collect the amount due.");
		$this->termsColumn($pdf, 2, 1, true, "10.3.");
		$this->termsColumn($pdf, 2, 2, false, "In the event of the credit provider instructing a firm of debt collectors or attorneys to collect the debt from the consumer, the credit provider shall be entitled to charge the consumer collection costs as provided for in clause 11 hereunder.");
		
		// end: column 2...
		
		// end: terms and conditions page 1...
		//
		//
		
		//
		//
		// terms and conditions page 2...
		
		$pdf->AddPage();
		
		// column 1...
		
		$pdf->SetY(16);
		
		$this->termsColumn($pdf, 1, 0, false, "11. BREACH AND LEGAL PROCEEDINGS FOR THE ENFORCEMENT OF THE AGREEMENT");
		$this->termsColumn($pdf, 1, 1, true, "11.1.");
		$this->termsColumn($pdf, 1, 2, false, "In the event of the following facts occurring");
		$this->termsColumn($pdf, 1, 2, true, "11.1.1.");
		$this->termsColumn($pdf, 1, 4, false, "the consumer failing to pay any amount due to the credit provider on the due date for such payment or breaching of any of the terms of this agreement; or");
		$this->termsColumn($pdf, 1, 2, true, "11.1.2.");
		$this->termsColumn($pdf, 1, 4, false, "the consumer being placed under provisional or final sequestration; or");
		$this->termsColumn($pdf, 1, 2, true, "11.1.3.");
		$this->termsColumn($pdf, 1, 4, false, "the consumer committing an act of insolvency as defined in the Insolvency  Act, 1936; or");
		$this->termsColumn($pdf, 1, 2, true, "11.1.4.");
		$this->termsColumn($pdf, 1, 4, false, "a judgment being granted against the consumer in respect of any debt which  remains unsatisfied for a period of seven days after the granting of such  judgment; or");
		$this->termsColumn($pdf, 1, 2, true, "11.1.5.");
		$this->termsColumn($pdf, 1, 4, false, "any property of the consumer being attached in execution of any debt;");
		$this->termsColumn($pdf, 1, 2, true, "11.1.6.");
		$this->termsColumn($pdf, 1, 4, false, "then and in any such event, the credit provider shall, without prejudice to any other rights which it may have in law, be entitled:");
		$this->termsColumn($pdf, 1, 5, false, "i) to claim immediate payment of the full balance owing by the consumer in terms of this agreement then owing by the consumer to the credit provider, including default administration charges and collection costs, where applicable; or");
		$this->termsColumn($pdf, 1, 5, false, "ii) if the consumer is in default under the credit agreement, to immediately cancel this agreement without notice, in which event the credit provider shall be entitled to recover possession of the goods from the consumer, to sell the goods, retain all amounts paid by the consumer and claim the full balance owing by the consumer in terms of this agreement, less the proceeds of the sale of the goods after the reasonable expenses in connection with the sale had been deducted.");
		$this->termsColumn($pdf, 1, 1, true, "11.2.");
		$this->termsColumn($pdf, 1, 2, false, "If the consumer is in default in terms of this agreement, the credit provider may draw the  default to the notice of the consumer in writing and propose that the consumer refer the  credit agreement to a debt counsellor, alternative dispute resolution agent, consumer court or ombudsman with jurisdiction, with the intent that the parties resolve any dispute  under the agreement or develop and agree on a plan to bring the payments under the  agreement up to date.");
		$this->termsColumn($pdf, 1, 1, true, "11.3.");
		$this->termsColumn($pdf, 1, 2, false, "Before instituting legal action for the enforcement of the agreement, the credit provider  shall comply with the provisions of the NCA.");
		$this->termsColumn($pdf, 1, 1, true, "11.4.");
		$this->termsColumn($pdf, 1, 2, false, "The credit provider shall be entitled before or after the institution of an action for the  recovery of possession of the goods sold in terms of this agreement to bring an  application for the attachment of the goods pending finalisation of such action.");
		
		$this->termsColumn($pdf, 1, 0, false, "12. DEFAULT ADMINISTRATION COSTS AND COLLECTION COSTS");
		$this->termsColumn($pdf, 1, 1, true, "12.1.");
		$this->termsColumn($pdf, 1, 2, false, "If the consumer defaults in any payment obligation under this agreement, the credit  provider may levy and the consumer will be obliged to pay such default administration charges as is permitted by the NCA and the regulations thereto. Such charges will be  equal to the charges payable in respect of a registered letter of demand in an undefended action in terms of the Magistrates Court Act together with necessary expenses incurred in  delivering the letter.");
		$this->termsColumn($pdf, 1, 1, true, "12.2.");
		$this->termsColumn($pdf, 1, 2, false, "The credit provider will be entitled to charge the consumer collection costs in respect of  the enforcement by the credit provider of the consumer's payment obligations under  this agreement. These collection costs will not exceed the costs incurred by the credit provider in collecting the debt");
		$this->termsColumn($pdf, 1, 2, true, "12.2.1.");
		$this->termsColumn($pdf, 1, 4, false, "to the extent limited by Part C of Chapter 6 of the NCA; and");
		$this->termsColumn($pdf, 1, 2, true, "12.2.2.");
		$this->termsColumn($pdf, 1, 4, false, "in terms of:");
		$this->termsColumn($pdf, 1, 4, false, "i) the Supreme Court Act, 1959 and the rules of the Court;");
		$this->termsColumn($pdf, 1, 4, false, "ii) the Magistrates Courts Act, 1944 and the rules of the Court;");
		$this->termsColumn($pdf, 1, 4, false, "iii) the Attorneys Act, 1979; and");
		$this->termsColumn($pdf, 1, 4, false, "iv) the Debt Collectors Act 1998, whichever is applicable to the enforcement of the credit agreement.");
		$this->termsColumn($pdf, 1, 1, true, "12.3.");
		$this->termsColumn($pdf, 1, 2, false, "Subject to the Magistrates Courts Act and the Supreme Court Act (including the rules  thereto) any legal costs payable by the consumer referred to in the previous paragraph  shall be on the attorney and client scale.");
		
		$this->termsColumn($pdf, 1, 0, false, "13. CERTIFICATE OF BALANCE");
		$this->termsColumn($pdf, 1, 0, false, "A certificate purporting to be signed by any manager of the credit provider, whose appointment  and position need not be proved, shall constitute sufficient evidence of the amount due to the  credit provider by the consumer, as well as any other fact mentioned therein, unless the amount  of the indebtedness or such other fact is rebutted by the consumer on a preponderance of probability. As such, the certificate shall constitute prima facie evidence.");
		
		$this->termsColumn($pdf, 1, 0, false, "14. CONSUMER'S RIGHT TO TERMINATE THE AGREEMENT AND SURRENDER THE GOODS");
		$this->termsColumn($pdf, 1, 1, true, "14.1.");
		$this->termsColumn($pdf, 1, 2, false, "The consumer may give written notice to the credit provider to terminate this  agreement and:");
		$this->termsColumn($pdf, 1, 2, true, "14.1.1.");
		$this->termsColumn($pdf, 1, 4, false, "if the goods are in the credit provider's possession, require the credit provider to sell the goods, or");
		$this->termsColumn($pdf, 1, 2, true, "14.1.2.");
		$this->termsColumn($pdf, 1, 4, false, "otherwise return the goods to the credit provider's place of business during ordinary business hours within five business days after the date of the notice or within such other period or at such other time or place as may be agreed with the  credit provider.");
		$this->termsColumn($pdf, 1, 1, true, "14.2.");
		$this->termsColumn($pdf, 1, 2, false, "Within ten business days after either receiving a notice in terms of clause 14.1.1 or receiving the goods in terms of clause 14.1.2,the credit provider shall give the consumer written notice setting out the  estimated value of the goods.");
		$this->termsColumn($pdf, 1, 1, true, "14.3.");
		$this->termsColumn($pdf, 1, 2, false, "Within ten business days after receiving a notice under clause 14.2, the consumer may  unconditionally withdraw the notice to terminate this agreement in writing and resume  possession of the goods, if the goods are in the credit provider's possession, unless the  consumer is in default under this agreement.");
		$this->termsColumn($pdf, 1, 1, true, "14.4.");
		$this->termsColumn($pdf, 1, 2, false, "If the consumer elects to withdraw the termination in terms of clause 14.3, the credit provider shall return the goods to the consumer unless the consumer is in default under  this agreement.");
		$this->termsColumn($pdf, 1, 1, true, "14.5.");
		$this->termsColumn($pdf, 1, 2, false, "If the consumer does not respond to a notice as contemplated in clause 14.3, the credit provider shall sell the goods as soon as practicable for the best price  reasonably obtainable.");
		$this->termsColumn($pdf, 1, 1, true, "14.6.");
		$this->termsColumn($pdf, 1, 2, false, "After selling any goods in terms of this clause, the credit provider shall:");
		$this->termsColumn($pdf, 1, 2, true, "14.6.1.");
		$this->termsColumn($pdf, 1, 4, false, "debit or credit the consumer's account with a payment or charge equivalent to  the proceeds of the sale, less any expenses reasonably incurred by the credit  provider in connection with the sale of the goods and");
		$this->termsColumn($pdf, 1, 2, true, "14.6.2.");
		$this->termsColumn($pdf, 1, 4, false, "give the consumer a written notice stating: -");
		$this->termsColumn($pdf, 1, 5, false, "i) the settlement value of the agreement immediately before the sale;");
		$this->termsColumn($pdf, 1, 5, false, "ii) the gross amount realised on the sale;");
		$this->termsColumn($pdf, 1, 5, false, "iii) the net proceeds of the sale after deducting the credit provider's permitted default charges (if applicable) and reasonable costs allowed under clause 14.6.1;");
		$this->termsColumn($pdf, 1, 5, false, "iv) the amount credited or debited to the consumer's account.");
		$this->termsColumn($pdf, 1, 1, true, "14.7.");
		$this->termsColumn($pdf, 1, 2, false, "If an amount is credited to the consumer's account and it exceeds the settlement amount, immediately before the sale, the credit provider shall pay the amount by which it exceeds  the settlement amount to the consumer and upon such payment this agreement shall  be terminated.");
		$this->termsColumn($pdf, 1, 1, true, "14.8.");
		$this->termsColumn($pdf, 1, 2, false, "If an amount is credited to the consumer's account and it is less than the settlement  amount immediately before the sale or an amount is debited to the consumer's account, the credit provider may demand payment from the consumer of the remaining  settlement value.");
		
		// end: column 1...
		
		// column 2...
		
		$pdf->SetY(16);
		
		$this->termsColumn($pdf, 2, 1, true, "14.9.");
		$this->termsColumn($pdf, 2, 2, false, "If the consumer fails to pay an amount demanded in terms of clause");
		$this->termsColumn($pdf, 2, 1, true, "14.8");
		$this->termsColumn($pdf, 2, 2, false, "within ten  business days of receiving a demand notice, the credit provider may commence  proceedings in terms of the Magistrates' Courts Act, 1944, to obtain a judgment for  payment of the balance due by the consumer.");
		$this->termsColumn($pdf, 2, 1, true, "14.10.");
		$this->termsColumn($pdf, 2, 2, false, "If the consumer, having received a demand notice, pays the amount demanded at any  time before judgment is obtained in terms of clause 14.9, this agreement shall be  terminated upon payment of that amount.");
		$this->termsColumn($pdf, 2, 1, true, "14.11.");
		$this->termsColumn($pdf, 2, 2, false, "Interest shall be payable by the consumer at the rate applicable to this agreement on any  outstanding amount demanded by the credit provider in terms of this clause from the date  of the demand until the date that the outstanding amount is paid.");
		
		$this->termsColumn($pdf, 2, 0, false, "15. CESSION AND ASSIGNMENT");
		$this->termsColumn($pdf, 2, 1, true, "15.1.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer shall not cede, assign or transfer any of the rights or obligations in terms of  this agreement without prior written consent of the credit provider.");
		$this->termsColumn($pdf, 2, 1, true, "15.2.");
		$this->termsColumn($pdf, 2, 2, false, "The credit provider shall be entitled, subject to the NCA, to cede, assign and/or transfer its rights and obligations under this agreement and its ownership of the goods. The  consumer agrees that upon such cession, assignment or transfer, he/she shall hold the  goods on the basis that the ownership in the goods has passed to the cessionary,  assignee or transferee.");
		
		$this->termsColumn($pdf, 2, 0, false, "16. ADDRESSES FOR RECEIVING OF DOCUMENTS");
		$this->termsColumn($pdf, 2, 1, true, "16.1.");
		$this->termsColumn($pdf, 2, 2, false, "All process (including summonses), documents, pleadings and notices relating to this  agreement shall be served or given to the credit provider at 6 Eastern Service Road, Eastgate, Sandton, 2090 (and no other address) unless such address has been changed  in writing as provided hereunder.");
		$this->termsColumn($pdf, 2, 1, true, "16.2.");
		$this->termsColumn($pdf, 2, 2, false, "All process (including summonses), documents, pleadings and notices relating to this agreement may be served or given to the consumer at the address mentioned in the  schedule to agreement, unless such address has been changed in writing as  provided hereunder.");
		$this->termsColumn($pdf, 2, 1, true, "16.3.");
		$this->termsColumn($pdf, 2, 2, false, "As such, the parties choose the aforesaid addresses as their respective domicilia citandi  (an address where service of the aforesaid documents may be effected).");
		$this->termsColumn($pdf, 2, 1, true, "16.4.");
		$this->termsColumn($pdf, 2, 2, false, "Either party to this agreement may change their address by delivering to the other party a written notice of the new address by hand, registered mail or electronic mail.");
		
		$this->termsColumn($pdf, 2, 0, false, "17. DISPUTE RESOLUTION");
		$this->termsColumn($pdf, 2, 0, false, "The consumer may resolve a complaint by way of alternative dispute resolution, file a complaint  with the National Credit Regulator, or make an application to the National Consumer Tribunal.");
		
		$this->termsColumn($pdf, 2, 0, false, "18. DEBT COUNSELLING");
		$this->termsColumn($pdf, 2, 0, false, "In terms of section 86 of the NCA the consumer may apply to a debt counsellor, in the  prescribed manner and form, to be declared over-indebted.");
		
		$this->termsColumn($pdf, 2, 0, false, "19. CREDIT INFORMATION");
		$this->termsColumn($pdf, 2, 1, true, "19.1.");
		$this->termsColumn($pdf, 2, 2, false, "In terms of Section 81 of the NCA the credit provider is obligated to take all reasonable  steps to prevent the extension of reckless credit. The consumer therefore authorises the credit  provider, as follows, with regard to the consumer's personal credit information, notwithstanding  the confidentiality of such information:");
		$this->termsColumn($pdf, 2, 3, false, "i) to enquire regarding the consumer's credit profile and repayment behaviour from any credit bureau, credit register or other credit provider;");
		$this->termsColumn($pdf, 2, 3, false, "ii) to supply or submit information regarding the consumer's credit profile or repayment  behavior to any credit bureau, credit register or other credit providers;");
		$this->termsColumn($pdf, 2, 3, false, "iii) to disclose the above information as required in law;");
		$this->termsColumn($pdf, 2, 3, false, "iv) to retain records of the consumer's personal and credit information in any data base in accordance with the provisions of the NCA.");
		$this->termsColumn($pdf, 2, 1, true, "19.2.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer has the right to contact the credit bureau, to have his or her credit record  disclosed and to have inaccurate information corrected.");
		
		$this->termsColumn($pdf, 2, 0, false, "20. CONTACT DETAILS");
		$this->termsColumn($pdf, 2, 0, false, "The following contact details are hereby disclosed:");
		$this->termsColumn($pdf, 2, 0, false, "Customer Care: 010 211 1120");
		$this->termsColumn($pdf, 2, 0, false, "National Credit Regulator: 086 062 7627");
		$this->termsColumn($pdf, 2, 0, false, "Credit Ombudsman: 086 1662 837");
		$this->termsColumn($pdf, 2, 0, false, "Credit Bureau: 086 1128 364");
		
		$this->termsColumn($pdf, 2, 0, false, "21. ADDITIONAL SERVICES");
		$this->termsColumn($pdf, 2, 0, false, "It is recorded that the credit provider does not require the consumer to become a member of  any consumer club offered by the credit provider, nor is it a requirement of this agreement that  such membership be taken up. The consumer may voluntarily elect to become a member of any of consumer club offered, in which case the benefits attaching to such membership shall  be regarded as an additional service and any consumer club fee payable will not form part of this credit agreement. The applicable consumer club fee shall be indicated on the schedule to this agreement.");
		
		$this->termsColumn($pdf, 2, 0, false, "22. INDULGENCE");
		$this->termsColumn($pdf, 2, 0, false, "No relaxation or indulgence granted by the credit provider to the consumer will be deemed to  be a waiver of any of the credit provider's rights in terms hereof, and such relaxation and  indulgence will not be deemed a novation of any of the terms and conditions of this agreement,  or create any estoppel against the credit provider. Any such indulgence, leniency or extension  granted will not amount to a breach of any of the terms of this agreement by the credit provider.");
		
		$this->termsColumn($pdf, 2, 0, false, "23. PROTECTION OF PERSONAL INFORMATION");
		$this->termsColumn($pdf, 2, 1, true, "23.1.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer gives consent to the credit provider to collect and/or process the  consumer's personal information insofar as it is necessary for the credit provider to fulfil its  obligations under this agreement.");
		$this->termsColumn($pdf, 2, 1, true, "23.2.");
		$this->termsColumn($pdf, 2, 2, false, "The credit provider shall, at all times, take reasonable steps to ensure that appropriate  security measures are in place for the protection of the integrity and confidentiality of the  consumer's personal information collected and/or processed by the credit provider.");
		
		$this->termsColumn($pdf, 2, 0, false, "24. GENERAL");
		$this->termsColumn($pdf, 2, 1, true, "24.1.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer confirms that this agreement is in one of the official languages that he/  she understands and acknowledges that he/she received a copy of the agreement in an  official language which he/she requested.");
		$this->termsColumn($pdf, 2, 1, true, "24.2.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer confirms and acknowledges that the terms and conditions of this  agreement, including the Schedule and his/her obligations there under, have been  explained to him/her and that he/she understands them in full and agrees to them.");
		$this->termsColumn($pdf, 2, 1, true, "24.3.");
		$this->termsColumn($pdf, 2, 2, false, "References in this agreement to the singular shall include the plural and vice versa and  references to the masculine gender shall include the feminine and neuter genders and  vice versa.");
		$this->termsColumn($pdf, 2, 1, true, "24.4.");
		$this->termsColumn($pdf, 2, 2, false, "Each clause of this agreement shall be severable from the remainder of the agreement  and should any clause in this agreement be found to be invalid, such invalidity shall not  affect the validity of the remainder of the agreement.");
		$this->termsColumn($pdf, 2, 1, true, "24.5.");
		$this->termsColumn($pdf, 2, 2, false, "This agreement, including and consisting of the schedule, the terms and conditions of the  agreement, the declaration of income, expenses and financial obligations by the  consumer, the affordability assessment and the credit application, constitutes the entire  agreement between the parties and no amendment, alteration, consensual cancellation or  waiver shall be valid or binding on the parties unless reduced to writing and signed by the  consumer and an authorised representative of the credit provider.");
		$this->termsColumn($pdf, 2, 1, true, "24.6.");
		$this->termsColumn($pdf, 2, 2, false, "The consumer hereby warrants that the information supplied in the application for credit  regarding his/her marital status is correct.");
		$this->termsColumn($pdf, 2, 1, true, "24.7.");
		$this->termsColumn($pdf, 2, 2, false, "In the event of the consumer being married in community of property, he/she warrants  that his/her spouse has consented to the conclusion of this agreement in writing and that  his/her signature has been attested to by two witnesses.");
		$this->termsColumn($pdf, 2, 1, true, "24.8.");
		$this->termsColumn($pdf, 2, 2, false, "This contract complies with the Commissioner's direction under section 20(7) of the Value Added Tax Act 89 of 1991.");
		
		// end: column 2...
		
		// end: terms and conditions page 2...
		//
		//
		
		//
		//
		// consent page...
		
		$pdf->AddPage();
		
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->setY(20);
		
		$pdf->SetFont('Arial', 'I', 12);
		$pdf->Cell(0, 0, '* You don\'t need to complete this page if you are not married in community of property', 0, 1, 'C');
		
		$pdf->SetLineWidth(1.5);
		$pdf->Line(60, 32, 150, 32);
		
		$pdf->setY(50);
		$pdf->SetFont('Arial', 'BU', 13);
		$pdf->Cell(0, 0, 'CONSENT BY SPOUSE MARRIED IN COMMUNITY OF PROPERTY', 0, 1, 'C');
		
		$pdf->setY(56);
		$pdf->SetFont('Arial', 'U', 10);
		$pdf->Cell(0, 0, 'In terms of section 15(2)(f) of the Matrimonial Property Act, 88 of 1984', 0, 1, 'C');
		
		$pdf->SetLineWidth(0.2);
		
		$pdf->SetY(75);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(0, 0, 'Name of Credit Applicant:', 0, 0, 'L');
		$pdf->Line(54, 76, 103, 76);
		$pdf->SetY(75);
		$pdf->SetX(105);
		$pdf->Cell(0, 0, 'Account Number:', 0, 0, 'L');
		$pdf->Line(136, 76, 200, 76);
		
		$pdf->SetY(90);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(0, 0, 'Details of Spouse:', 0, 0, 'L');
		
		$pdf->SetY(97);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(0, 0, 'Name:', 0, 0, 'L');
		$pdf->Line(24, 98, 103, 98);
		$pdf->SetY(97);
		$pdf->SetX(105);
		$pdf->Cell(0, 0, 'Surname:', 0, 0, 'L');
		$pdf->Line(123, 98, 200, 98);
		
		$pdf->SetY(104);
		$pdf->Cell(0, 0, 'Address:', 0, 0, 'L');
		$pdf->Line(27, 105, 103, 105);
		$pdf->Line(10, 112, 103, 112);
		$pdf->SetY(104);
		$pdf->SetX(105);
		$pdf->Cell(0, 0, 'Telephone:', 0, 0, 'L');
		$pdf->Line(125, 105, 200, 105);
		$pdf->Line(125, 102, 125, 105);
		$pdf->Line(132.5, 102, 132.5, 105);
		$pdf->Line(140, 102, 140, 105);
		$pdf->Line(147.5, 102, 147.5, 105);
		$pdf->Line(155, 102, 155, 105);
		$pdf->Line(162.5, 102, 162.5, 105);
		$pdf->Line(170, 102, 170, 105);
		$pdf->Line(177.5, 102, 177.5, 105);
		$pdf->Line(185, 102, 185, 105);
		$pdf->Line(192.5, 102, 192.5, 105);
		$pdf->Line(200, 102, 200, 105);
		
		$pdf->SetY(111);
		$pdf->SetX(105);
		$pdf->Cell(0, 0, 'Cell Phone:', 0, 0, 'L');
		$pdf->Line(125, 112, 200, 112);
		$pdf->Line(125, 109, 125, 112);
		$pdf->Line(132.5, 109, 132.5, 112);
		$pdf->Line(140, 109, 140, 112);
		$pdf->Line(147.5, 109, 147.5, 112);
		$pdf->Line(155, 109, 155, 112);
		$pdf->Line(162.5, 109, 162.5, 112);
		$pdf->Line(170, 109, 170, 112);
		$pdf->Line(177.5, 109, 177.5, 112);
		$pdf->Line(185, 109, 185, 112);
		$pdf->Line(192.5, 109, 192.5, 112);
		$pdf->Line(200, 109, 200, 112);
		
		$pdf->SetY(126);
		$pdf->Cell(0, 0, 'Declaration:', 0, 0, 'L');
		
		$pdf->SetY(133);
		$pdf->Cell(0, 0, 'I, the undersigned,', 0, 0, 'L');
		$pdf->Line(42, 134, 142, 134);
		$pdf->SetY(133);
		$pdf->Cell(0, 0, 'married in community of property to', 0, 0, 'R');
		
		$pdf->SetY(140);
		$pdf->Cell(0, 0, '(referred to in this document as the Credit Applicant) hereby give written', 0, 0, 'R');
		$pdf->Line(10, 141, 85, 141);
		
		$pdf->SetY(147);
		$pdf->Cell(0, 0, 'consent for my spouse, as the Credit Applicant, to conclude a Credit Agreement with JDG Trading (Pty) Ltd on our', 0, 0, 'L');
		
		$pdf->SetY(154);
		$pdf->Cell(0, 0, 'behalf.', 0, 0, 'L');
		
		$pdf->SetY(169);
		$pdf->Cell(0, 0, 'Thus done and signed at:', 0, 0, 'L');
		
		$pdf->SetY(184);
		$pdf->Line(10, 180, 200, 180);
		$pdf->Cell(0, 0, '(Full address where document was signed)', 0, 0, 'L');
		
		$pdf->SetY(191);
		$pdf->Cell(0, 0, 'On the', 0, 0, 'L');
		$pdf->Line(23, 192, 50, 192);
		$pdf->SetY(191);
		$pdf->SetX(50);
		$pdf->Cell(0, 0, 'day of', 0, 0, 'L');
		$pdf->Line(62, 192, 110, 192);
		$pdf->SetY(191);
		$pdf->SetX(110);
		$pdf->Cell(0, 0, '20', 0, 0, 'L');
		$pdf->Line(116, 192, 124, 192);
		
		$pdf->SetY(213);
		$pdf->Cell(0, 0, 'Signature', 0, 0, 'L');
		$pdf->Line(10, 210, 65, 210);
		
		$pdf->SetY(235);
		$pdf->Cell(0, 0, 'Name of witness', 0, 0, 'L');
		$pdf->Line(10, 232, 65, 232);
		$pdf->SetY(235);
		$pdf->SetX(70);
		$pdf->Cell(0, 0, 'Signature', 0, 0, 'L');
		$pdf->Line(71, 232, 126, 232);
		
		$pdf->SetY(242);
		$pdf->Cell(0, 0, 'Address:', 0, 0, 'L');
		$pdf->Line(27, 243, 103, 243);
		$pdf->Line(10, 250, 103, 250);
		$pdf->SetY(242);
		$pdf->SetX(105);
		$pdf->Cell(0, 0, 'Telephone:', 0, 0, 'L');
		$pdf->Line(125, 243, 200, 243);
		$pdf->Line(125, 240, 125, 243);
		$pdf->Line(132.5, 240, 132.5, 243);
		$pdf->Line(140, 240, 140, 243);
		$pdf->Line(147.5, 240, 147.5, 243);
		$pdf->Line(155, 240, 155, 243);
		$pdf->Line(162.5, 240, 162.5, 243);
		$pdf->Line(170, 240, 170, 243);
		$pdf->Line(177.5, 240, 177.5, 243);
		$pdf->Line(185, 240, 185, 243);
		$pdf->Line(192.5, 240, 192.5, 243);
		$pdf->Line(200, 240, 200, 243);
		
		$pdf->SetY(249);
		$pdf->SetX(105);
		$pdf->Cell(0, 0, 'Cell Phone:', 0, 0, 'L');
		$pdf->Line(125, 250, 200, 250);
		$pdf->Line(125, 247, 125, 250);
		$pdf->Line(132.5, 247, 132.5, 250);
		$pdf->Line(140, 247, 140, 250);
		$pdf->Line(147.5, 247, 147.5, 250);
		$pdf->Line(155, 247, 155, 250);
		$pdf->Line(162.5, 247, 162.5, 250);
		$pdf->Line(170, 247, 170, 250);
		$pdf->Line(177.5, 247, 177.5, 250);
		$pdf->Line(185, 247, 185, 250);
		$pdf->Line(192.5, 247, 192.5, 250);
		$pdf->Line(200, 247, 200, 250);
		
		// end: consent page...
		//
		//
		
		
		
		
		
		$pdf->Output();
		
	}
}