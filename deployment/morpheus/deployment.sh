#!/bin/bash

cd /var/www/html/rochester/
git checkout master
git config core.fileMode false
git remote update

if git status | grep -q "diverged\|ahead\|behind"; then

	# some configurations...
	shopt -s dotglob
	backup_filename="/var/www/html/git-backups/rochester-untracked-$(date +%s).tar"

	# backup any modified and untracked files...
	git ls-files --modified -z | xargs -0 tar rvf $backup_filename
	git ls-files --others --exclude-standard -z | xargs -0 tar rvf $backup_filename

	# sync up with the remote...
	git reset --hard
	git fetch origin master
	git reset --hard origin/master

	# restore the untracked files...
	tar -xvf $backup_filename -C ./

	# backup the DB for the DevOps environment...
	/bin/rm ./deployment/devops/database/main.sql
	mysqldump -u devops -p'0114753496*!890' rochester_morpheus > ./deployment/devops/database/main.sql

	# stage and commit these changes to the 'master' branch...
	git config user.email "development@hellocavalry.com"
	git config user.name "Cavalry Media Build Bot"
	git add -A
	git commit -m "Cavalry Media DevOps Build - Rochester"
	git push -f origin master

	# DEVOPS-SPECIFIC THINGS
	# copy over any files we need, push them to the 'master-devops' branch, create an external tarball, and reset...
	/bin/cp -Rvf ./deployment/devops/files/* ./
	git add -A
	git commit -m "Cavalry Media DevOps Build - Rochester DevOps"
	git push -f origin master:master-devops
	# tar -czf /var/www/html/rochester/../latest-build-rochester-devops.tar.gz /var/www/html/rochester/
	git reset --hard origin/master

	# LIVE ENVIRONMENT-SPECIFIC THINGS
	# copy over any files we need, push them to the 'master-live' branch, create an external tarball, and reset...
	/bin/cp -Rvf ./deployment/live/files/* ./
	git add -A
	git commit -m "Cavalry Media DevOps Build - Rochester LIVE"
	git push -f origin master:master-live
	# tar -czf /var/www/html/rochester/../latest-build-rochester-live.tar.gz /var/www/html/rochester/
	git reset --hard origin/master

	# MORPHEUS-SPECIFIC THINGS
	# copy over any files we need...
	/bin/cp -Rvf ./deployment/morpheus/files/* ./

	# move our tarballs somewhere we can access them...
	# mv /var/www/html/rochester/../latest-build-rochester-devops.tar.gz ./
	# mv /var/www/html/rochester/../latest-build-rochester-live.tar.gz ./

	# set up the permissions...
	chown -R www-data:www-data ./
	chmod -R 755 ./

fi