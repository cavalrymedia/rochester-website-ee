#!/bin/bash

cd /var/www/html/rochester/
git checkout master-devops
git config core.fileMode false
git remote update

if git status | grep -q "diverged\|ahead\|behind"; then

	# sync up with the remote...
	git reset --hard
	git fetch origin master-devops
	git reset --hard origin/master-devops

	# restore the database...
	mysql -u devops -p'0114753496*!890' rochester < ./deployment/devops/database/main.sql

	# set up the permissions...
	chown -R www-data:www-data ./
	chmod -R 755 ./

fi